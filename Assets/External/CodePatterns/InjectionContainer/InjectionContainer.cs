using System;
using System.Collections.Generic;

namespace CodePatterns.InjectionContainer {
    public class InjectionContainer : IDisposable {
        private class InjectionHolder {
            public object Object;
            public string PropertyName;
            public Dictionary<int, bool> InjectedGeneratorHashes;
        }

        private LinkedList<IInjectionTarget> Targets = new LinkedList<IInjectionTarget>();
        private LinkedList<InjectionHolder> Injections = new LinkedList<InjectionHolder>();

        public InjectionContainer RegisterTarget(IInjectionTarget target) {
            if (!Targets.Contains(target)) {
                Targets.AddLast(target);
                InjectAllFieldsToTarget(target);
            }

            return this;
        }

        public InjectionContainer RegisterInjection(object variable, string propertyName) {
            foreach (var injection in Injections) {
                if (injection.PropertyName == propertyName) {
                    Injections.Remove(injection);
                    break;
                }
            }

            var newInjection = new InjectionHolder();
            newInjection.Object = variable;
            newInjection.PropertyName = propertyName;
            newInjection.InjectedGeneratorHashes = new Dictionary<int, bool>();
            Injections.AddLast(newInjection);

            InjectFieldToAllTargets(newInjection);

            return this;
        }

        private void InjectFieldToAllTargets(InjectionHolder injection) {
            foreach (var target in Targets) {
                var targetHashCode = target.GetHashCode();
                var targetType = target.GetType();
                var fieldInfos = targetType.GetFields();

                foreach (var fieldInfo in fieldInfos) {
                    if (fieldInfo.Name == injection.PropertyName) {
                        fieldInfo.SetValue(target, injection.Object);
                    }
                }
            }
        }

        private void InjectAllFieldsToTarget(IInjectionTarget target) {
            var targetHashCode = target.GetHashCode();
            var targetType = target.GetType();
            var fieldInfos = targetType.GetFields();

            foreach (var injection in Injections) {
                foreach (var fieldInfo in fieldInfos) {
                    if (fieldInfo.Name == injection.PropertyName) {
                        fieldInfo.SetValue(target, injection.Object);
                    }
                }
            }
        }

        public void Dispose() {
            foreach (var target in Targets) {
                if (target is IDisposable) {
                    ((IDisposable) target).Dispose();
                }
            }
            
            Targets = null;
            Injections = null;
        }

        ~InjectionContainer() {
            Dispose();
        }
    }
}