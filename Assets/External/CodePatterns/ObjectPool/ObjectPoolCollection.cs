using System.Collections.Generic;
using UnityEngine;

namespace CodePatterns.ObjectPool {
    public class ObjectPoolCollection<TKey, T> where T : PoolableObjectBase {
        private Dictionary<TKey, ObjectPool<T>> _poolCollection;
        private string _assetPathTemplate;

        public ObjectPoolCollection(string assetPathTemplate) {
            _poolCollection = new Dictionary<TKey, ObjectPool<T>>();
            _assetPathTemplate = assetPathTemplate;
        }

        public bool TryGet(TKey id, out ObjectPool<T> pool) {
            if (_poolCollection.TryGetValue(id, out var foundedPool)) {
                pool = foundedPool;
                return true;
            }
            var assetPath = string.Format(_assetPathTemplate, id);
            var prefab = Resources.Load(assetPath);
            if (prefab != null) {
                pool = new ObjectPool<T>(assetPath);
                if (_poolCollection.TryAdd(id, pool)) {
                    return true;
                }
            }
            pool = null;
            return false;
        }
    }
}