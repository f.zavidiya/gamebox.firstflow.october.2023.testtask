using System.Collections.Generic;
using UnityEngine;

namespace CodePatterns.ObjectPool {
    public class ObjectPool<T> : IObjectPool<T> where T : PoolableObjectBase {
        public GameObject Prefab {
            get {
                if (ObjectPrefab == null) {
                    ObjectPrefab = Resources.Load<GameObject>(AssetPath);
                }
                return ObjectPrefab;
            }
        }

        private string AssetPath;
        private GameObject ObjectPrefab;

        private Stack<T> storedObjects = new Stack<T>();

        public ObjectPool(string assetPath) {
            AssetPath = assetPath;
        }

        public T Get() {
            if (storedObjects.Count > 0) {
                var storedObject = storedObjects.Pop();
                storedObject.GetGameObject().SetActive(true);
                return storedObject;
            }

            if (Prefab == null) {
                return null;
            }

            var newObject = Object.Instantiate(Prefab);
            var objectInPoolComponent = newObject.GetComponent<T>();
            if (objectInPoolComponent == null) {
                objectInPoolComponent = newObject.AddComponent<T>();
            }
            objectInPoolComponent.Pool = this;

            objectInPoolComponent.Clear();

            return objectInPoolComponent;
        }

        public void Release(IPoolableObject target) {
            target.GetGameObject().transform.SetParent(null);
            target.GetGameObject().SetActive(false);
            target.Clear();
            storedObjects.Push((T) target);
        }
    }
}