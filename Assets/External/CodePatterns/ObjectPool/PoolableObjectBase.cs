using UnityEngine;

namespace CodePatterns.ObjectPool {
    public abstract class PoolableObjectBase : MonoBehaviour, IPoolableObject {
        public IObjectPool Pool { get; set; }

        public GameObject GetGameObject() {
            return gameObject;
        }

        public virtual void Clear() { }

        public void Release() {
            if (Pool == null) {
                DestroyImmediate(gameObject);
                return;
            }

            Pool.Release(this);
        }
    }
}