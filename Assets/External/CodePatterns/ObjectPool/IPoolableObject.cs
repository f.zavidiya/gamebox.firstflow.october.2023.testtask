using UnityEngine;

namespace CodePatterns.ObjectPool {
    public interface IPoolableObject {
        IObjectPool Pool { get; set; }
        GameObject GetGameObject();
        void Clear();
        void Release();
    }
}