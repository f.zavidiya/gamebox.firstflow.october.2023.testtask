namespace CodePatterns.ObjectPool {
    public interface IObjectPool {
        void Release(IPoolableObject target);
    }
    
    public interface IObjectPool<T> : IObjectPool where T : IPoolableObject {
        T Get();
        
    }
}