using CodePatterns.Automatization;
using CodePatterns.InjectionContainer;

namespace CodePatterns.AutoWrappers {
    public class AutoInjectionContainer : InjectionContainer.InjectionContainer, IAuto {
        public void AutoRegister(object system, Auto attribute) {
            if (system is IInjectionTarget) {
                RegisterTarget((IInjectionTarget) system);
            }
        }
    }
}