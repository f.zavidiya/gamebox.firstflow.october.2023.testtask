using CodePatterns.Automatization;
using CodePatterns.Events;

namespace CodePatterns.AutoWrappers {
    public class AutoEventBus : EventBus, IAuto {
        public void AutoRegister(object system, Auto attribute) {
            if (system is IEventConsumer) {
                RegisterEventConsumer((IEventConsumer) system);
            }
        }
    }
}