namespace CodePatterns.Automatization {
    public interface IAuto {
        void AutoRegister(object system, Auto attribute);
    }
}