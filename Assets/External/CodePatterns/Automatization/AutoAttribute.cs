namespace CodePatterns.Automatization {
    [System.AttributeUsage(System.AttributeTargets.Class)]
    public class Auto : System.Attribute {
        protected int[] _executionOrders;

        public int[] ExecutionOrders { get { return _executionOrders; } }

        public Auto() {
            _executionOrders = new int[] { 1000 };
        }

        public Auto(int[] executionOrders) {
            _executionOrders = executionOrders;
        }

        public Auto(int executionOrder) {
            _executionOrders = new int[] { executionOrder };
        }
    }
}