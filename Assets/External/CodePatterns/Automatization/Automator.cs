using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CodePatterns.Automatization {
    public class Automator : IDisposable {
        public class SystemOrderEntry {
            public int ExecutionOrder;
            public Type SystemType;
            public object SystemInstance;
            public Auto AutoAttribute;
        }

        private class SystemEntry {
            public int ExecutionOrder;
            public int SystemTypeIndex;
            public Auto AutoAttribute;
        }

        private List<IAuto> _automatizators = new List<IAuto>();
        
        private List<Type> _systemTypes = new List<Type>();
        private Dictionary<Type, object> _systemInstances = new Dictionary<Type, object>();
        private List<SystemEntry> _systems = new List<SystemEntry>();

        private Dictionary<int, List<int>> _executionOrder = new Dictionary<int, List<int>>();
        private int[] _executionOrderKeysSorted;

        public void Register(IAuto automatizator) {
            _automatizators.Add(automatizator);
        }

        /// <summary>
        /// Use [Auto] flag to class for auto-registration.
        /// </summary>
        public void Init() {
            var types = Assembly.GetExecutingAssembly().GetTypes();
            foreach (var type in types) {
                if (type.IsClass && HasAttribute<Auto>(type)) {
                    var auto = GetAttribute<Auto>(type);

                    _systemTypes.Add(type);
                    var typeIndex = _systemTypes.Count - 1;

                    foreach (var order in auto.ExecutionOrders) {
                        var systemEntry = new SystemEntry() {
                            ExecutionOrder = order,
                            SystemTypeIndex = typeIndex,
                            AutoAttribute = auto
                        };
                        _systems.Add(systemEntry);
                    }

                    var systemIndex = _systems.Count - 1;

                    foreach (var order in auto.ExecutionOrders) {
                        if (!_executionOrder.ContainsKey(order)) {
                            _executionOrder.Add(order, new List<int>());
                        }
                        _executionOrder[order].Add(systemIndex);
                    }
                }
            }

            _executionOrderKeysSorted = _executionOrder.Keys.ToArray();
            for (var i = _executionOrderKeysSorted.Length - 1; i > 0; i--) {
                for (var j = 0; j < i; j++) {
                    if (_executionOrderKeysSorted[j] > _executionOrderKeysSorted[j + 1]) {
                        (_executionOrderKeysSorted[j], _executionOrderKeysSorted[j + 1]) = (_executionOrderKeysSorted[j + 1], _executionOrderKeysSorted[j]);
                    }
                }
            }
        }

        public void Start() {
            foreach (var order in _executionOrderKeysSorted) {
                foreach (var systemIndex in _executionOrder[order]) {
                    var system = _systems[systemIndex];
                    var systemType = _systemTypes[system.SystemTypeIndex];
                    if (!_systemInstances.ContainsKey(systemType)) {
                        var newSystemInstance = Activator.CreateInstance(systemType);
                        _systemInstances.Add(systemType, newSystemInstance);
                        foreach (var automatizator in _automatizators) {
                            automatizator.AutoRegister(newSystemInstance, system.AutoAttribute);
                        }
                    }
                }
            }
        }

        public List<SystemOrderEntry> GetExectionOrder() {
            var executionOrderList = new List<SystemOrderEntry>();
            foreach (var order in _executionOrderKeysSorted) {
                foreach (var systemIndex in _executionOrder[order]) {
                    var system = _systems[systemIndex];
                    var systemType = _systemTypes[system.SystemTypeIndex];
                    var systemOrderEntry = new SystemOrderEntry() {
                            ExecutionOrder = system.ExecutionOrder,
                            SystemType = systemType,
                            AutoAttribute = system.AutoAttribute
                    };
                    if (_systemInstances.TryGetValue(systemType, out var systemInstance)) {
                        systemOrderEntry.SystemInstance = systemInstance;
                    }
                    executionOrderList.Add(systemOrderEntry);
                }
            }
            return executionOrderList;
        }

        private T GetAttribute<T>(Type type) where T : Attribute {
            var findingType = typeof(T);
            return (T) type.GetCustomAttribute(findingType);
        }

        private bool HasAttribute<T>(Type type) where T : Attribute {
            return GetAttribute<T>(type) != null;
        }

        public void Dispose() {
            foreach (var automatizator in _automatizators) {
                if (automatizator is IDisposable) {
                    ((IDisposable) automatizator).Dispose();
                }
            }
        }
    }
}