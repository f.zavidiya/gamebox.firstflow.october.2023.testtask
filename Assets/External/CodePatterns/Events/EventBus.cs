using System;
using System.Collections.Generic;

namespace CodePatterns.Events {
    public class EventBus : IEventBus, IDisposable {
        private Dictionary<Type, List<WeakReference<IEventConsumer>>> _consumers = new Dictionary<Type, List<WeakReference<IEventConsumer>>>();
        private Stack<IEvent> _deferredEvents = new Stack<IEvent>();
        private Dictionary<Type, Stack<IEvent>> _eventPool = new Dictionary<Type, Stack<IEvent>>();

        /// <summary>
        /// Get free typed event instance from pool.
        /// </summary>
        /// <typeparam name="T">Event type realizing IEvent interface.</typeparam>
        /// <returns>Free typed event instance.</returns>
        public T Get<T>() where T : new() {
            var type = typeof(T);
            if (_eventPool.TryGetValue(type, out var storedEvent) && storedEvent.Count > 0) {
                return (T) storedEvent.Pop();
            }
            var newEvent = new T();
            return newEvent;
        }

        /// <summary>
        /// Add event instance to pool for recycle.
        /// </summary>
        /// <param name="releasingEvent">Unusual event instance.</param>
        public void Release(IEvent releasingEvent) {
            var type = releasingEvent.GetType();
            if (!_eventPool.ContainsKey(type)) {
                _eventPool.Add(type, new Stack<IEvent>());
            }
            _eventPool[type].Push(releasingEvent);
        }

        /// <summary>
        /// Send event to all consumers.
        /// </summary>
        /// <typeparam name="T">Type of raising event. Must realize IEvent interface.</typeparam>
        /// <param name="newEvent">Event must be get from EventBus.Get<T>().</param>
        /// <returns>Self</returns>
        public IEventBus RaiseEvent<T>(T newEvent) where T : IEvent {
            var eventType = newEvent.GetType();
            if (_consumers.TryGetValue(eventType, out var consumersList)) {
                foreach (var consumerReference in consumersList) {
                    if (consumerReference.TryGetTarget(out var consumer)) {
                        ((IEventConsumer<T>)consumer).CatchEvent(newEvent);
                    }
                }
            }
            if (newEvent is not IManualReleaseEvent) {
                Release(newEvent);
            }
            return this;
        }

        /// <summary>
        /// Add event to queue. Event must be sent to all consumers at call EventBus.RunDeferredEvents() method.
        /// </summary>
        /// <typeparam name="T">Type of raising event. Must realize IEvent interface.</typeparam>
        /// <param name="newEvent">Event must be get from EventBus.Get<T>().</param>
        /// <returns>Self</returns>
        public IEventBus RaiseDeferredEvent<T>(T newEvent) where T : IEvent {
            _deferredEvents.Push(newEvent);
            return this;
        }

        /// <summary>
        /// Send all events from deferred queue to consumers.
        /// </summary>
        /// <returns>Self</returns>
        public IEventBus RunDeferredEvents() {
            while (_deferredEvents.Count > 0) {
                var deferredEvent = _deferredEvents.Pop();
                RaiseEvent(deferredEvent);
            }
            return this;
        }

        public IEventBus RegisterEventConsumer(IEventConsumer consumer) {
            var methods = consumer.GetType().GetMethods();
            foreach (var method in methods) {
                if (method.Name.CompareTo("CatchEvent") == 0) {
                    var parameters = method.GetParameters();
                    if (parameters.Length == 1) {
                        var eventType = parameters[0].ParameterType;
                        var consumerUntypized = (IEventConsumer) consumer;
                        if (!_consumers.ContainsKey(eventType)) {
                            _consumers.Add(eventType, new List<WeakReference<IEventConsumer>>());
                        }
                        var consumerList = _consumers[eventType];
                        var found = false;
                        for (var i = 0; i < consumerList.Count; i++) {
                            if (consumerList[i].TryGetTarget(out var entryConsumer) && entryConsumer == consumerUntypized) {
                                found = true;
                            }
                        }
                        if (!found) {
                            consumerList.Add(new WeakReference<IEventConsumer>(consumerUntypized));
                        }
                    }
                }
            }
            return this;
        }

        public IEventBus RegisterEventConsumer<T>(IEventConsumer<T> consumer) where T: IEvent {
            var consumerUntypized = (IEventConsumer) consumer;
            var eventType = typeof(T);
            if (!_consumers.ContainsKey(eventType)) {
                _consumers.Add(eventType, new List<WeakReference<IEventConsumer>>());
            }
            var consumerList = _consumers[eventType];
            for (var i = 0; i < consumerList.Count; i++) {
                if (consumerList[i].TryGetTarget(out var entryConsumer) && entryConsumer == consumerUntypized) {
                    return this;
                }
            }
            consumerList.Add(new WeakReference<IEventConsumer>(consumerUntypized));
            return this;
        }

        public IEventBus ReleaseEventConsumer<T>(IEventConsumer<T> consumer) where T: IEvent {
            var eventType = typeof(T);
            if (_consumers.ContainsKey(eventType)) {
                var consumerList = _consumers[eventType];
                for (var i = 0; i < consumerList.Count; i++) {
                    consumerList.RemoveAt(i);
                }
            }
            return this;
        }

        public void Dispose() {
            foreach (var consumerList in _consumers) {
                foreach (var consumerReference in consumerList.Value) {
                    if (consumerReference.TryGetTarget(out var consumer)) {
                        if (consumer is IDisposable) {
                            ((IDisposable) consumer).Dispose();
                        }
                    }
                }
            }

            _consumers = null;
            _deferredEvents = null;
            _eventPool = null;
        }
    }
}