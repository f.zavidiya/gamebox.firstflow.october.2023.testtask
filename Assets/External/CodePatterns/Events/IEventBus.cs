namespace CodePatterns.Events {
    public interface IEventBus {
        IEventBus RaiseEvent<T>(T newEvent) where T : IEvent;
        IEventBus RaiseDeferredEvent<T>(T newEvent) where T : IEvent;
        IEventBus RunDeferredEvents();
        IEventBus RegisterEventConsumer(IEventConsumer consumer);
        IEventBus RegisterEventConsumer<T>(IEventConsumer<T> consumer) where T: IEvent;
        IEventBus ReleaseEventConsumer<T>(IEventConsumer<T> consumer) where T: IEvent;
    }
}