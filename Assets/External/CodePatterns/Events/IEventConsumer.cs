namespace CodePatterns.Events {
    public interface IEventConsumer {}

    public interface IEventConsumer<T> {
        void CatchEvent(T currentEvent);
    }
}