namespace CodePatterns.Serialization {
    public class ByteBuffer {
        public byte[] Data;
        public int Pointer;
    }
}
