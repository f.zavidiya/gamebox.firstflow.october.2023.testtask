#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System;

namespace Scripts.Storages {
    [CustomEditor(typeof(ItemsStorage))]
    public class ItemsStorageEditor : Editor {
        ItemsStorage t;
        SerializedObject _target;
        SerializedProperty _list;
        SerializedProperty _list2;
        int _listSize;
        int _list2Size;
    
        void OnEnable(){
            t = (ItemsStorage) target;
            _target = new SerializedObject(t);
            _list = _target.FindProperty("Items");
            _list2 = _target.FindProperty("ShopOffers");
        }
    
        public override void OnInspectorGUI(){
            _target.Update();
    
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Items", EditorStyles.foldoutHeader);
            _listSize = _list.arraySize;
            _listSize = EditorGUILayout.IntField ("", _listSize, GUILayout.Width(60f));
            EditorGUILayout.EndHorizontal();

            if(_listSize != _list.arraySize){
                while(_listSize > _list.arraySize){
                    _list.InsertArrayElementAtIndex(_list.arraySize);
                }
                while(_listSize < _list.arraySize){
                    _list.DeleteArrayElementAtIndex(_list.arraySize - 1);
                }
            }
    
            EditorGUILayout.Space();
    
            for(int i = 0; i < _list.arraySize; i++){
                var _itemRef = _list.GetArrayElementAtIndex(i);
                var _typeOfItem = _itemRef.FindPropertyRelative("TypeOfItem");
                var _iconSprite = _itemRef.FindPropertyRelative("IconSprite");
                var _name = _itemRef.FindPropertyRelative("Name");
                var _description = _itemRef.FindPropertyRelative("Description");
                var _unique = _itemRef.FindPropertyRelative("Unique");
                var _assetPath = _itemRef.FindPropertyRelative("AssetPath");
                var _price = _itemRef.FindPropertyRelative("SellPrice");
                var _biomesList = _itemRef.FindPropertyRelative("BiomeSpawnChance");

                GUILayout.BeginHorizontal();

                GUILayout.BeginVertical();
                EditorGUILayout.LabelField($"{i + 1}.", EditorStyles.boldLabel, GUILayout.Width(40f));
                if(GUILayout.Button("X", GUILayout.Width(40f))){
                    _list.DeleteArrayElementAtIndex(i);
                } else {
                    GUILayout.EndVertical();

                    GUILayout.BeginVertical(EditorStyles.inspectorFullWidthMargins);
                    EditorGUILayout.PropertyField(_typeOfItem);
                    EditorGUILayout.PropertyField(_iconSprite);
                    EditorGUILayout.PropertyField(_name);
                    EditorGUILayout.PropertyField(_description);
                    EditorGUILayout.PropertyField(_unique);
                    EditorGUILayout.PropertyField(_assetPath);
                    EditorGUILayout.LabelField($" - /Assets/Game/Resources/Prefabs/{_assetPath.stringValue}");
                    EditorGUILayout.PropertyField(_price);

                    GUILayout.Label($"Biome Spawn Chances: ", EditorStyles.boldLabel);
                    for(int j = 0; j < _biomesList.arraySize; j++){
                        var _biomeChance = _biomesList.GetArrayElementAtIndex(j);
                        GUILayout.BeginHorizontal();
    
                        GUILayout.Label($" - {((Enums.BiomeType)j).ToString()} ");
    
                        if (float.TryParse(EditorGUILayout.TextField(_biomeChance.floatValue.ToString()), out float value)) {
                            _biomeChance.floatValue = value;
                        }
    
                        if(GUILayout.Button("X")) {
                            t.Items[i].BiomeSpawnChance.RemoveAt(j);
                        }

                        GUILayout.EndHorizontal();
                    }
                }
                GUILayout.EndVertical();

                GUILayout.EndHorizontal();

                if(GUILayout.Button("+")) {
                    if (t.Items[i].BiomeSpawnChance.Count < ((int[])Enum.GetValues(typeof(Enums.BiomeType))).Length) {
                        t.Items[i].BiomeSpawnChance.Add(0f);
                    }
                }

                EditorGUILayout.Space();
            }

            if(GUILayout.Button("New")) {
                t.Items.Add(new ItemsStorage.Item());
            }

            // ------

            EditorGUILayout.Space();
            EditorGUILayout.Space();

            // ------

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Shop Offers", EditorStyles.foldoutHeader);
            _list2Size = _list2.arraySize;
            _list2Size = EditorGUILayout.IntField("", _list2Size, GUILayout.Width(60f));
            EditorGUILayout.EndHorizontal();

            if (_list2Size != _list2.arraySize){
                while(_list2Size > _list2.arraySize){
                    _list2.InsertArrayElementAtIndex(_list2.arraySize);
                }
                while(_list2Size < _list2.arraySize){
                    _list2.DeleteArrayElementAtIndex(_list2.arraySize - 1);
                }
            }
    
            EditorGUILayout.Space();

    
            for (int i = 0; i < _list2.arraySize; i++){
                var _itemRef = _list2.GetArrayElementAtIndex(i);
                var _typeOfItem = _itemRef.FindPropertyRelative("TypeOfItem");
                var _iconSprite = _itemRef.FindPropertyRelative("IconSprite");
                var _name = _itemRef.FindPropertyRelative("Name");
                var _amount = _itemRef.FindPropertyRelative("Amount");
                var _cost = _itemRef.FindPropertyRelative("Cost");
        
                GUILayout.BeginHorizontal();

                GUILayout.BeginVertical();
                EditorGUILayout.LabelField($"{i + 1}.", EditorStyles.boldLabel, GUILayout.Width(40f));
                if(GUILayout.Button("X", GUILayout.Width(40f))){
                    _list2.DeleteArrayElementAtIndex(i);
                } else {
                    GUILayout.EndVertical();

                    GUILayout.BeginVertical(EditorStyles.inspectorFullWidthMargins);
                    EditorGUILayout.PropertyField(_typeOfItem);
                    EditorGUILayout.PropertyField(_iconSprite);
                    EditorGUILayout.PropertyField(_name);
                    EditorGUILayout.PropertyField(_amount);
                    EditorGUILayout.PropertyField(_cost);
                }
                GUILayout.EndVertical();

                GUILayout.EndHorizontal();

                EditorGUILayout.Space();
            }

            if(GUILayout.Button("New")){
                t.ShopOffers.Add(new ItemsStorage.Offer());
            }
    
            _target.ApplyModifiedProperties();
        }
    }
}
#endif
