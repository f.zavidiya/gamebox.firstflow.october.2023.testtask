using CodePatterns.Automatization;
using CodePatterns.Events;
using CodePatterns.InjectionContainer;
using Scripts.Events;
using Scripts.Storages;
using Scripts.Structures;
using Scripts.UnityComponents;
using UnityEngine.Scripting;

namespace Scripts.Systems {
    [Auto(9)] [Preserve]
    public class InitItems : IInjectionTarget, IEventConsumer, IEventConsumer<StartEvent> {
        public GameStoredState GameState;
        public ItemsStorage ItemsStorage;
        public HUDView HUDView;

        public void CatchEvent(StartEvent e) {
            HUDView.InitItems(ItemsStorage.Items);
            HUDView.InitOffers(ItemsStorage.ShopOffers, ItemsStorage.Items);
        }
    }
}