using CodePatterns.Automatization;
using CodePatterns.Events;
using CodePatterns.InjectionContainer;
using CodePatterns.ObjectPool;
using Scripts.Events;
using Scripts.Structures;
using Scripts.UnityComponents;
using UnityEngine;
using UnityEngine.Scripting;

namespace Scripts.Systems {
    [Auto(99)] [Preserve]
    public class VehicleMarker : IInjectionTarget, IEventConsumer, IEventConsumer<FixedUpdateEvent> {
        public GameStoredState GameStoredState;
        public GameRuntimeState GameRuntimeState;
        public Camera MainCamera;
        public HUDView HUDView;

        public ObjectPool<WorldMarkerIconView> UIVehicleMarkerPool;

        public void CatchEvent(FixedUpdateEvent e) {
            if (GameRuntimeState.UIVehicleIcon == null) {
                var newIcon = UIVehicleMarkerPool.Get();
                
                newIcon.transform.SetParent(HUDView.transform);
                newIcon.transform.localScale = Vector3.one;
                newIcon.gameObject.SetActive(false);
                GameRuntimeState.UIVehicleIcon = newIcon;
            }

            var icon = GameRuntimeState.UIVehicleIcon;
            if (icon != null && icon) {
                if (GameRuntimeState.VehicleQueue.First != null) {
                    var isActive = GameStoredState.CollectedObjects.Count > 0;
                    if (icon.gameObject.activeSelf != isActive) {
                        icon.gameObject.SetActive(isActive);
                    }
                    if (isActive) {
                        var vehicle = GameRuntimeState.VehicleQueue.First.Value;
                        icon.UpdatePosition(vehicle.transform.position + Vector3.up);
                        icon.UpdateValue(vehicle.Load / (float) vehicle.Capacity);
                        icon.UpdateTime(vehicle.RestUntilTime - Time.time);
                    }
                }
            }
        }
    }
}