using CodePatterns.Automatization;
using CodePatterns.Events;
using CodePatterns.InjectionContainer;
using Scripts.Events;
using Scripts.Structures;
using Scripts.UnityComponents;
using UnityEngine.Scripting;

namespace Scripts.Systems {
    [Auto(200)] [Preserve]
    public class UpdateUIValues : IInjectionTarget, IEventConsumer, IEventConsumer<FixedUpdateEvent>, IEventConsumer<CollectGatherableEvent>, IEventConsumer<ChangePerkEvent>, IEventConsumer<ChangeCashEvent>, IEventConsumer<BasketBlockedEvent> {
        public GameStoredState GameStoredState;
        public GameRuntimeState GameRuntimeState;
        public HUDView HUDView;

        public void CatchEvent(FixedUpdateEvent e) {
            HUDView.SetStats(GameStoredState, GameRuntimeState);
        }

        public void CatchEvent(CollectGatherableEvent e) {
            HUDView.SetGatherableCounter(e.ItemType, GameStoredState.GetCollectedObjectAmount(e.ItemType), e.Position);
            HUDView.UpdateBasketCounter(GameStoredState.GetTotalItemsAmount());

        }

        public void CatchEvent(ChangePerkEvent e) {
            HUDView.SetPerkCounter(e.ItemType, GameStoredState.GetItemAmount(e.ItemType), e.Position);
            HUDView.UpdateOffers(GameStoredState.Inventory);
            HUDView.UpdateBasketCapacity(GameRuntimeState.Player.BasketCapacity);
        }

        public void CatchEvent(ChangeCashEvent e) {
            HUDView.SetCash(GameStoredState.Cash);
        }

        public void CatchEvent(BasketBlockedEvent e) {
            HUDView.ShowBasketBlocked();
        }
    }
}