using CodePatterns.Automatization;
using CodePatterns.Events;
using CodePatterns.InjectionContainer;
using CodePatterns.ObjectPool;
using Scripts.Events;
using Scripts.Storages;
using Scripts.Structures;
using Scripts.UnityComponents;
using UnityEngine;
using UnityEngine.Scripting;

namespace Scripts.Systems {
    [Auto(99)] [Preserve]
    public class ShopInteractions : IInjectionTarget, IEventConsumer, IEventConsumer<FixedUpdateEvent> {
        public GameStoredState GameStoredState;
        public GameRuntimeState GameRuntimeState;
        public EventBus EventBus;
        public Camera MainCamera;
        public HUDView HUDView;
        public ConstantsStorage ConstantsStorage;
        
        public ObjectPool<ShopView> ShopPool;

        private ShopView _lastShop;

        public void CatchEvent(FixedUpdateEvent e) {
            if (GameRuntimeState.Shop == null) {
                GameRuntimeState.Shop = ShopPool.Get();
                GameRuntimeState.Shop.transform.position = ConstantsStorage.ShopPositionOffset + Vector3.forward * ConstantsStorage.ShopFirstAppearingDistance;
            }

            while (GameRuntimeState.Shop.transform.position.z < GameRuntimeState.LastGeneratedGatherableZ - ConstantsStorage.ShopAppearingDistancePeriodic) {
                GameRuntimeState.Shop.transform.position += Vector3.forward * ConstantsStorage.ShopAppearingDistancePeriodic;
            }
            
            ShopView shop = null;
            foreach (var collider in GameRuntimeState.Player.LatestCollisions) {
                if (collider.transform.root.TryGetComponent<ShopView>(out var shopView)) {
                    shop = shopView;
                }
            }

            if (_lastShop != shop) {
                if (_lastShop != null) {
                    _lastShop.CloseShop();
                }

                _lastShop = shop;
                if (_lastShop != null) {
                    HUDView.UpdateOffers(GameStoredState.Inventory);
                    shop.OpenShop();
                }
            }

            if (GameRuntimeState.Shop.IsOpened) {
                HUDView.ShowGoods();
                foreach (var offer in HUDView.CalledOffers) {
                    if (GameStoredState.Cash >= offer.Cost && (!offer.Unique || !GameStoredState.HasItem(offer.ItemType))) {
                        GameStoredState.Cash -= offer.Cost;
                        GameStoredState.AddItem(offer.ItemType, offer.Amount);
                        HUDView.SetCash(GameStoredState.Cash);
                        HUDView.UpdateOffers(GameStoredState.Inventory);

                        var changePerkEvent = EventBus.Get<ChangePerkEvent>();
                        changePerkEvent.ItemType = offer.ItemType;
                        changePerkEvent.Position = GameRuntimeState.Player.transform.position;
                        EventBus.RaiseEvent(changePerkEvent);

                        var soundEvent = EventBus.Get<PlaySoundEvent>();
                        soundEvent.IsMusic = false;
                        soundEvent.SoundId = 3;
                        soundEvent.AttachedTo = null;
                        soundEvent.Position = shop.transform.position;
                        EventBus.RaiseEvent(soundEvent);
                    }
                }
                HUDView.CalledOffers.Clear();
            } else {
                HUDView.HideGoods();
            }
        }
    }
}