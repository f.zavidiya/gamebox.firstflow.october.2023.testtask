using CodePatterns.Automatization;
using CodePatterns.Events;
using CodePatterns.InjectionContainer;
using Scripts.Events;
using Scripts.Structures;
using Scripts.UnityComponents;
using UnityEngine.Scripting;

namespace Scripts.Systems {
    [Auto(99)] [Preserve]
    public class UpdateMessageBubble : IInjectionTarget, IEventConsumer, IEventConsumer<SetMessagePositionEvent>, IEventConsumer<ShowMessageEvent>, IEventConsumer<HideMessageEvent> {
        public GameStoredState GameState;
        public HUDView HUDView;

        public void CatchEvent(SetMessagePositionEvent e) {
            HUDView.MessageBubble.SetWorldAnchor(e.Position);
        }

        public void CatchEvent(ShowMessageEvent e) {
            HUDView.MessageBubble.SetMessage(e.MessageText);
            HUDView.MessageBubble.gameObject.SetActive(true);
        }

        public void CatchEvent(HideMessageEvent e) {
            HUDView.MessageBubble.gameObject.SetActive(false);
        }
    }
}