using CodePatterns.Automatization;
using CodePatterns.Events;
using CodePatterns.InjectionContainer;
using Scripts.Events;
using Scripts.UnityComponents;
using UnityEngine.Scripting;

namespace Scripts.Systems {
    [Auto(100)] [Preserve]
    public class MainMenuInteractions : IInjectionTarget, IEventConsumer, IEventConsumer<OpenMainMenuEvent> {
        public HUDView HUDView;

        public void CatchEvent(OpenMainMenuEvent e) {
            HUDView.SwitchMainMenu();
        }
    }
}