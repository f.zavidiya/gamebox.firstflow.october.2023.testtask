using CodePatterns.Automatization;
using CodePatterns.Events;
using CodePatterns.InjectionContainer;
using Scripts.Enums;
using Scripts.Events;
using Scripts.Storages;
using Scripts.Structures;
using UnityEngine;
using UnityEngine.Scripting;

namespace Scripts.Systems {
    [Auto(100)] [Preserve]
    public class BuddyDialog : IInjectionTarget, IEventConsumer, IEventConsumer<SkipInputEvent> {
        public GameStoredState GameStoredState;
        public GameRuntimeState GameRuntimeState;
        public EventBus EventBus;
        public ConstantsStorage ConstantsStorage;
        public MessagesStorage MessagesStorage;

        private int _currentMessageQueueId;
        private int _currentMessageQueueStep;
        private int _messagesPerIdleState;

        private const int _tutorialStepMoving = 1;
        private const int _tutorialStepBoots = 2;
        private const int _tutorialStepRabbits = 4;

        public void CatchEvent(SkipInputEvent e) {
            if (e.Hide) {
                if (e.StateChanged) {
                    _currentMessageQueueId = 0;
                    _currentMessageQueueStep = 0;
                }

                EventBus.RaiseEvent(EventBus.Get<HideMessageEvent>());
            } else {
                if (e.StateChanged) {
                    if (_currentMessageQueueId == 0) {
                        _currentMessageQueueStep = 0;
                    }

                    _messagesPerIdleState = 0;
                }

                ShowNextDialogStep();
            }
        }

        private void ShowNextDialogStep() {
            var buddy = GameRuntimeState.Buddy;
            if (buddy == null) {
                return;
            }

            if (buddy.BehaviorState != AnimalBehaviorState.Idle) {
                return;
            }

            _messagesPerIdleState++;

            if (_messagesPerIdleState >= ConstantsStorage.MessagesCountToBuddyRetreat) {
                buddy.LastBehaviorStateChangeTime = Time.time + ConstantsStorage.BuddyCooldownAfterEndOfDialog - ConstantsStorage.BuddyBehaviorChangePeriod;
                buddy.BehaviorState = AnimalBehaviorState.Retreat;

                EventBus.RaiseEvent(EventBus.Get<HideMessageEvent>());

                return;
            }

            var message = GetDependMessage();
            if (message == null) {
                message = GetRandomMessage();
            }

            if (message != null) {
                var showMessageEvent = EventBus.Get<ShowMessageEvent>();
                showMessageEvent.MessageText = message.Text;
                EventBus.RaiseEvent(showMessageEvent);
            } else {
                EventBus.RaiseEvent(EventBus.Get<HideMessageEvent>());

                _currentMessageQueueId = 0;
                _currentMessageQueueStep = 0;
            }
        }

        private MessagesStorage.Message GetRandomMessage() {
            MessagesStorage.Message message = null;
            if (_currentMessageQueueStep == 0) {
                message = MessagesStorage.GetRandomBuddyMessage();
                _currentMessageQueueId = message.Id;
            }

            return message;
        }

        private MessagesStorage.Message GetDependMessage() {
            var messageQueueId = 0;

            if ((_currentMessageQueueId == 0 || _currentMessageQueueId ==_tutorialStepMoving)
            && (GameStoredState.TutorialState & _tutorialStepMoving) == 0
            && GameStoredState.DistanceTraveled < 1f) {
                messageQueueId = DialogNextStep(_tutorialStepMoving);
            }

            if ((_currentMessageQueueId == 0 || _currentMessageQueueId ==_tutorialStepRabbits)
            && (GameStoredState.TutorialState & _tutorialStepRabbits) == 0
            && GameStoredState.DistanceTraveled > 100f) {
                messageQueueId = DialogNextStep(_tutorialStepRabbits);
            }

            if ((_currentMessageQueueId == 0 || _currentMessageQueueId ==_tutorialStepBoots)
            && (GameStoredState.TutorialState & _tutorialStepBoots) == 0
            && GameStoredState.DistanceTraveled > 200f) {
                messageQueueId = DialogNextStep(_tutorialStepBoots);
            }

            if (messageQueueId == 0) {
                return null;
            }

            var message = MessagesStorage.GetBuddyMessageById(_currentMessageQueueId, _currentMessageQueueStep);

            if (message == null && _currentMessageQueueId > 0) {
                GameStoredState.TutorialState |= _currentMessageQueueId;
                _currentMessageQueueId = 0;
                _currentMessageQueueStep = 0;
            }

            return message;
        }

        private int DialogNextStep(int id) {
            if (_currentMessageQueueId != id) {
                _currentMessageQueueId = id;
                _currentMessageQueueStep = 0;
            } else {
                _currentMessageQueueStep++;
            }
            return _currentMessageQueueId;
        }
    }
}