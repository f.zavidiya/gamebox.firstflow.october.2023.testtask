using CodePatterns.Automatization;
using CodePatterns.Events;
using CodePatterns.InjectionContainer;
using CodePatterns.ObjectPool;
using Scripts.Enums;
using Scripts.Events;
using Scripts.Storages;
using Scripts.Structures;
using Scripts.UnityComponents;
using UnityEngine;
using UnityEngine.Scripting;

namespace Scripts.Systems {
    [Auto(100)] [Preserve]
    public class RabbitBehavior : IInjectionTarget, IEventConsumer, IEventConsumer<FixedUpdateEvent> {
        public GameStoredState GameStoredState;
        public GameRuntimeState GameRuntimeState;
        public ConstantsStorage ConstantsStorage;

        public ObjectPool<AnimalView> RabbitPool;
        public ObjectPool<EffectView> PickUpEffectPool;
        
        public void CatchEvent(FixedUpdateEvent e) {
            while (GameRuntimeState.Rabbits.Count < ConstantsStorage.RabbitsCap) {
                var rabbit = RabbitPool.Get();
                rabbit.Index = GameRuntimeState.Rabbits.Count;
                var position = GameRuntimeState.Player.transform.position 
                    + new Vector3(Random.Range(-ConstantsStorage.RabbitAreaBounds.x / 2f, ConstantsStorage.RabbitAreaBounds.x / 2f), 0f, -15f);
                position.y = 0;
                rabbit.transform.position = position;
                rabbit.LastBehaviorStateChangeTime = Random.Range(0f, -ConstantsStorage.RabbitBehaviorChangePeriod);
                GameRuntimeState.Rabbits.AddLast(rabbit);
            }

            var node = GameRuntimeState.Rabbits.First;
            while (node != null) {
                var nodeNext = node.Next;
                var rabbit = node.Value;
                
                if (Time.time - rabbit.LastBehaviorStateChangeTime > ConstantsStorage.RabbitBehaviorChangePeriod) {
                    rabbit.LastBehaviorStateChangeTime = Time.time;

                    if (rabbit.BehaviorState == AnimalBehaviorState.Retreat) {
                        rabbit.Target = null;
                        rabbit.BehaviorState = AnimalBehaviorState.Idle;
                        rabbit.IsScared = false;
                    }

                    rabbit.BehaviorState = (AnimalBehaviorState) (((int) rabbit.BehaviorState) + 1);
                    
                    if (rabbit.BehaviorState == AnimalBehaviorState.ReturnToIdle) {
                        rabbit.BehaviorState = AnimalBehaviorState.Idle;
                    }

                    if (rabbit.Target != null && !rabbit.Target.gameObject.activeSelf) {
                        rabbit.Target = null;
                    }

                    if (rabbit.BehaviorState == AnimalBehaviorState.Idle) {
                        if (rabbit.Target != null && rabbit.Target.gameObject.activeSelf && (rabbit.Target.position - rabbit.transform.position).magnitude > 30f) {
                            rabbit.BehaviorState = AnimalBehaviorState.MovingToTarget;
                        }

                        if (rabbit.IsTamed) {
                            if (rabbit.CarryingItemType != ItemType.None) {
                               rabbit.Target = GameRuntimeState.Player.Transform;
                            }
                            rabbit.BehaviorState = AnimalBehaviorState.MovingToTarget;
                        } else {
                            rabbit.CarryingItemType = ItemType.None;
                        }
                    }
                    
                    if (rabbit.BehaviorState == AnimalBehaviorState.MovingToTarget) {
                        if (rabbit.Target == null) {
                            var l = 0;
                            foreach (var gatherable in GameRuntimeState.Gatherables) {
                                if (gatherable.gameObject.activeSelf) {
                                    l++;
                                }
                            }
                            
                            var index = Random.Range(0, l);
                            
                            var j = 0;
                            foreach (var gatherable in GameRuntimeState.Gatherables) {
                                if (gatherable.gameObject.activeSelf) {
                                    if (j == index) {
                                        rabbit.Target = gatherable.transform;
                                        break;
                                    }
                                    j++;
                                }
                            }

                            if (rabbit.Target == null) {
                                rabbit.BehaviorState = AnimalBehaviorState.Idle;
                            }
                        }
                    }
                }

                node = nodeNext;
            }
        }
    }
}