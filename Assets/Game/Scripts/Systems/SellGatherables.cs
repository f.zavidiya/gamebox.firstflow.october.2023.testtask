using System.Linq;
using CodePatterns.Automatization;
using CodePatterns.Events;
using CodePatterns.InjectionContainer;
using Scripts.Events;
using Scripts.Storages;
using Scripts.Structures;
using Scripts.UnityComponents;
using UnityEngine;
using UnityEngine.Scripting;

namespace Scripts.Systems {
    [Auto(99)] [Preserve]
    public class SellGatherables : IInjectionTarget, IEventConsumer, IEventConsumer<FixedUpdateEvent> {
        public GameStoredState GameStoredState;
        public GameRuntimeState GameRuntimeState;
        public EventBus EventBus;
        public Camera MainCamera;
        public ItemsStorage ItemsStorage;

        private const float TransferActionCooldown = 0.2f;

        public void CatchEvent(FixedUpdateEvent e) {
            if (Time.time - GameRuntimeState.Player.LastTransferActionTime > TransferActionCooldown && GameStoredState.CollectedObjects.Count > 0) {
                GameRuntimeState.Player.LastTransferActionTime = Time.time;
                var node = GameRuntimeState.Player.LatestCollisions.First;
                while (node != null) {
                    var nodeNext = node.Next;
                    var collider = node.Value;
                    if (collider.transform.root.TryGetComponent<VehicleView>(out var vehicle)) {
                        var totalPrice = 0;

                        if (GameStoredState.CollectedObjects.Count > 0) {
                            var key = GameStoredState.CollectedObjects.Keys.First();
                            var amount = GameStoredState.CollectedObjects[key] - 1;
                            if (amount > 0) {
                                GameStoredState.CollectedObjects[key] = amount;
                            } else {
                                GameStoredState.CollectedObjects.Remove(key);
                            }
                            
                            var itemDescription = ItemsStorage.GetItemByType(key);
                            if (itemDescription != null) {
                                totalPrice += itemDescription.SellPrice;
                            }

                            GameRuntimeState.Player.SetCollectedGatherables(GameStoredState.CollectedObjects);
                            GameStoredState.Cash += totalPrice;

                            var collectEvent = EventBus.Get<CollectGatherableEvent>();
                            collectEvent.ItemType = key;
                            collectEvent.Position = vehicle.transform.position;
                            EventBus.RaiseEvent(collectEvent);

                            EventBus.RaiseEvent(EventBus.Get<ChangeCashEvent>());

                            vehicle.Load += 1;
                            if (vehicle.Load >= vehicle.Capacity) {
                                vehicle.SetActive(false);
                                GameRuntimeState.VehicleQueue.Remove(vehicle);
                                GameRuntimeState.VehicleQueue.AddLast(vehicle);
                                GameRuntimeState.Player.LatestCollisions.Remove(collider);

                                var vehicleSoundEvent = EventBus.Get<PlaySoundEvent>();
                                vehicleSoundEvent.IsMusic = false;
                                vehicleSoundEvent.SoundId = 8;
                                vehicleSoundEvent.AttachedTo = vehicle.transform;
                                vehicleSoundEvent.Position = vehicle.transform.position;
                                EventBus.RaiseEvent(vehicleSoundEvent);
                            }

                            var soundEvent = EventBus.Get<PlaySoundEvent>();
                            soundEvent.IsMusic = false;
                            soundEvent.SoundId = 1;
                            soundEvent.AttachedTo = null;
                            soundEvent.Position = vehicle.transform.position;
                            EventBus.RaiseEvent(soundEvent);
                        }
                    }
                    node = nodeNext;
                }
            }
        }
    }
}