using CodePatterns.Automatization;
using CodePatterns.Events;
using CodePatterns.InjectionContainer;
using Scripts.Events;
using Scripts.Structures;
using UnityEngine.Scripting;

namespace Scripts.Systems {
    [Auto(99)] [Preserve]
    public class ReleaseEffects : IInjectionTarget, IEventConsumer, IEventConsumer<FixedUpdateEvent> {
        public GameRuntimeState GameRuntimeState;

        public void CatchEvent(FixedUpdateEvent e) {
            var node = GameRuntimeState.ActiveEffects.First;
            while (node != null) {
                var nodeNext = node.Next;
                var effect = node.Value;
                if (!effect.Active) {
                    effect.Release();
                    GameRuntimeState.ActiveEffects.Remove(node);
                }
                node = nodeNext;
            }
        }
    }
}