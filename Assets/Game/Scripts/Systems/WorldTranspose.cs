using CodePatterns.Automatization;
using CodePatterns.Events;
using CodePatterns.InjectionContainer;
using Scripts.Events;
using Scripts.Storages;
using Scripts.Structures;
using UnityEngine;
using UnityEngine.Scripting;

namespace Scripts.Systems {
    [Auto(101)] [Preserve]
    public class WorldTranspose : IInjectionTarget, IEventConsumer, IEventConsumer<LoadEvent>, IEventConsumer<FixedUpdateEvent> {
        public GameRuntimeState GameRuntimeState;
        public Camera MainCamera;
        public ConstantsStorage ConstantsStorage;

        public void CatchEvent(LoadEvent e) {
            GameRuntimeState.Sign.SetValue(GameRuntimeState.Sign.transform.position.z + GameRuntimeState.GlobalOffsetZ);
        }

        public void CatchEvent(FixedUpdateEvent e) {
            if (GameRuntimeState.Player != null && GameRuntimeState.Player.Transform.position.z > ConstantsStorage.WorldTransposeStep - 15f) {
                var transposeDelta = new Vector3(0f, 0f, -ConstantsStorage.WorldTransposeStep);
                GameRuntimeState.GlobalOffsetZ += ConstantsStorage.WorldTransposeStep;
                GameRuntimeState.LastGeneratedGatherableZ -= ConstantsStorage.WorldTransposeStep;
                GameRuntimeState.Player.Transform.position += transposeDelta;
                MainCamera.transform.position += transposeDelta;
                GameRuntimeState.Shop.transform.position += transposeDelta;
                foreach (var gatherable in GameRuntimeState.Gatherables) {
                    gatherable.transform.position += transposeDelta;
                }
                foreach (var rabbit in GameRuntimeState.Rabbits) {
                    rabbit.transform.position += transposeDelta;
                }
                foreach (var vehicle in GameRuntimeState.VehicleQueue) {
                    vehicle.transform.position += transposeDelta;
                }
                foreach (var chunk in GameRuntimeState.GroundChunks) {
                    chunk.transform.position += transposeDelta;
                }
                foreach (var chunk in GameRuntimeState.RoadChunks) {
                    chunk.transform.position += transposeDelta;
                }
                foreach (var chunk in GameRuntimeState.PineChunks) {
                    chunk.transform.position += transposeDelta;
                }
                GameRuntimeState.Sign.SetValue(GameRuntimeState.Sign.transform.position.z + GameRuntimeState.GlobalOffsetZ);
            }
        }
    }
}