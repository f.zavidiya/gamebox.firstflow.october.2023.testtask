using CodePatterns.Automatization;
using CodePatterns.Events;
using CodePatterns.InjectionContainer;
using CodePatterns.ObjectPool;
using Scripts.Events;
using Scripts.Structures;
using Scripts.UnityComponents;
using UnityEngine.Scripting;

namespace Scripts.Systems {
    [Auto(8)] [Preserve]
    public class SpawnPlayer : IInjectionTarget, IEventConsumer, IEventConsumer<StartEvent> {
        public GameRuntimeState GameRuntimeState;
        
        public ObjectPool<CharacterView> PlayerCharacterPool;
        
        public void CatchEvent(StartEvent e) {
            GameRuntimeState.Player = PlayerCharacterPool.Get();
        }
    }
}