using CodePatterns.Automatization;
using CodePatterns.Events;
using CodePatterns.InjectionContainer;
using CodePatterns.ObjectPool;
using Scripts.Enums;
using Scripts.Events;
using Scripts.Storages;
using Scripts.Structures;
using Scripts.UnityComponents;
using UnityEngine;
using UnityEngine.Scripting;

namespace Scripts.Systems {
    [Auto(100)] [Preserve]
    public class AIMove : IInjectionTarget, IEventConsumer, IEventConsumer<FixedUpdateEvent> {
        public GameStoredState GameStoredState;
        public GameRuntimeState GameRuntimeState;
        public EventBus EventBus;
        public ConstantsStorage ConstantsStorage;

        public ObjectPool<AnimalView> RabbitPool;
        public ObjectPool<EffectView> PickUpEffectPool;
        
        public void CatchEvent(FixedUpdateEvent e) {
            var node = GameRuntimeState.Rabbits.First;
            while (node != null) {
                var nodeNext = node.Next;
                Move(node.Value);
                node = nodeNext;
            }

            Move(GameRuntimeState.Buddy);
        }

        private void Move(AIView unit) {
            if (unit == null) {
                return;
            }

            if (unit.BehaviorState == AnimalBehaviorState.Idle) {
                unit.SetVelocity(Vector3.zero);
            } else if (unit.BehaviorState == AnimalBehaviorState.MovingToTarget) {
                if (unit.Target != null && unit.Target.gameObject.activeSelf) {
                    var targetPosition = unit.Target.position;
                    if (unit.Target.Equals(GameRuntimeState.Player.Transform)) {
                        if (unit.Index == -1) {
                            targetPosition += (unit.transform.position - targetPosition).normalized * (GameRuntimeState.Player.IsFull ? unit.ConsumingDistance * 2f : (unit.ConsumingDistance - 0.1f));
                        } else {
                            targetPosition += Quaternion.Euler(0f, 127.3f * unit.Index, 0f) * Vector3.forward * (GameRuntimeState.Player.IsFull ? unit.ConsumingDistance * 2f : (unit.ConsumingDistance - 0.1f));
                        }
                    }
                    var direction = targetPosition - unit.transform.position;
                    direction.y = 0f;
                    var directionNormalized = direction.normalized;
                    var velocity = directionNormalized * unit.Speed;
                    if (unit is AnimalView) {
                        var animal = unit as AnimalView;
                        if (animal.IsTamed) {
                            velocity *= GameRuntimeState.PetsSpeedMultiplier;
                        }
                    }
                    if (direction.magnitude > 0.05f) {
                        if (direction.magnitude > 30f) {
                            velocity *= 2f;
                        }

                        unit.transform.position += directionNormalized * Mathf.Min(velocity.magnitude * Time.fixedDeltaTime, direction.magnitude);

                        if (!Vector3.zero.Equals(directionNormalized)) {
                            var newRotation = Quaternion.LookRotation(directionNormalized, Vector3.up);
                            unit.transform.rotation = Quaternion.Slerp(unit.transform.rotation, newRotation, Time.fixedDeltaTime / ConstantsStorage.RotationSmoothApplyTime);
                        }
                    } else {
                        velocity *= 0f;
                    }

                    unit.SetVelocity(velocity);

                    ProcessUnitCollision(unit);
                } else {
                    unit.SetVelocity(Vector3.zero);
                }
            } else if (unit.BehaviorState == AnimalBehaviorState.Retreat) {
                var direction = Vector3.back * 30f;
                if (unit.Target != null && unit.Target.gameObject.activeSelf) {
                    var targetPosition = unit.Target.position;
                    direction = -(targetPosition - unit.transform.position);
                    direction.y = 0f;
                }
                var directionMagnitude = direction.magnitude;
                var directionNormalized = direction.normalized;
                var velocity = directionNormalized * unit.Speed;
                if (directionMagnitude > 0.05f && directionMagnitude < 10f) {
                    if (direction.magnitude < 3f) {
                        velocity *= 2f;
                    }

                    unit.transform.position += directionNormalized * Mathf.Min(velocity.magnitude * Time.fixedDeltaTime, direction.magnitude);

                    if (!Vector3.zero.Equals(directionNormalized)) {
                        var newRotation = Quaternion.LookRotation(directionNormalized, Vector3.up);
                        unit.transform.rotation = Quaternion.Slerp(unit.transform.rotation, newRotation, Time.fixedDeltaTime / ConstantsStorage.RotationSmoothApplyTime);
                    }
                } else {
                    velocity *= 0f;
                }

                unit.SetVelocity(velocity);

                unit.LatestCollisions.Clear();
            }
        }

        private void ProcessUnitCollision(AIView unit) {
            var hash = unit.Target.gameObject.GetHashCode();
            if (unit is AnimalView) {
                if ((unit.transform.position - unit.Target.position).magnitude < unit.ConsumingDistance) {
                    if (!unit.IsFull && GameRuntimeState.GatherablesByHash.TryGetValue(hash, out var gatherable)) {
                        unit.Target = null;
                        unit.CarryingItemType = gatherable.ItemType;

                        var effect = PickUpEffectPool.Get();
                        effect.transform.position = gatherable.transform.position;
                        effect.SetLifetime(0.5f);
                        effect.Play();
                        GameRuntimeState.ActiveEffects.AddLast(effect);

                        gatherable.Release();
                        GameRuntimeState.GatherablesByHash.Remove(hash);
                        GameRuntimeState.Gatherables.Remove(gatherable);
                    } else if (unit.Target.Equals(GameRuntimeState.Player.Transform) && !GameRuntimeState.Player.IsFull) {
                        unit.LastBehaviorStateChangeTime = Time.time - 10f;
                        unit.BehaviorState = AnimalBehaviorState.Idle;
                        if (unit.CarryingItemType != ItemType.None) {
                            GameStoredState.AddCollectedObject(unit.CarryingItemType, 1);
                            GameRuntimeState.Player.SetCollectedGatherables(GameStoredState.CollectedObjects);

                            var collectEvent = EventBus.Get<CollectGatherableEvent>();
                            collectEvent.ItemType = unit.CarryingItemType;
                            collectEvent.Position = unit.transform.position;
                            EventBus.RaiseEvent(collectEvent);

                            unit.CarryingItemType = ItemType.None;

                            var soundEvent = EventBus.Get<PlaySoundEvent>();
                            soundEvent.IsMusic = false;
                            soundEvent.SoundId = 1;
                            soundEvent.AttachedTo = null;
                            soundEvent.Position = unit.transform.position;
                            EventBus.RaiseEvent(soundEvent);
                        }
                        unit.Target = null;
                    }
                }
            }
        }
    }
}