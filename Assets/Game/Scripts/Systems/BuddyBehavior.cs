using CodePatterns.Automatization;
using CodePatterns.Events;
using CodePatterns.InjectionContainer;
using CodePatterns.ObjectPool;
using DG.Tweening;
using Scripts.Enums;
using Scripts.Events;
using Scripts.Storages;
using Scripts.Structures;
using Scripts.UnityComponents;
using UnityEngine;
using UnityEngine.Scripting;

namespace Scripts.Systems {
    [Auto(100)] [Preserve]
    public class BuddyBehavior : IInjectionTarget, IEventConsumer, IEventConsumer<FixedUpdateEvent> {
        public GameRuntimeState GameRuntimeState;
        public EventBus EventBus;
        public Camera MainCamera;
        public ConstantsStorage ConstantsStorage;

        public ObjectPool<AIView> BuddyCharacterPool;

        private readonly Vector3 _startPositionOffset = new Vector3(-10f, 0f, -3f);

        public void CatchEvent(FixedUpdateEvent e) {
            if (GameRuntimeState.Buddy == null) {
                var newBuddy = BuddyCharacterPool.Get();
                var position = GameRuntimeState.Player.transform.position + _startPositionOffset;
                position.y = 0;
                newBuddy.transform.position = position;
                newBuddy.Target = GameRuntimeState.Player.transform;
                GameRuntimeState.Buddy = newBuddy;
            }

            var buddy = GameRuntimeState.Buddy;
            var lastState = buddy.BehaviorState;

            if (Time.time - buddy.LastBehaviorStateChangeTime > ConstantsStorage.BuddyBehaviorChangePeriod) {
                buddy.LastBehaviorStateChangeTime = Time.time;

                if (buddy.BehaviorState == AnimalBehaviorState.Retreat) {
                    buddy.BehaviorState = AnimalBehaviorState.Idle;
                }

                if (buddy.BehaviorState == AnimalBehaviorState.KnockDown) {
                    buddy.LastBehaviorStateChangeTime = Time.time + ConstantsStorage.BuddyCooldownAfterStrike - ConstantsStorage.BuddyBehaviorChangePeriod;
                    buddy.BehaviorState = (AnimalBehaviorState) ((int) AnimalBehaviorState.Retreat - 1);
                }

                buddy.BehaviorState = (AnimalBehaviorState) (((int) buddy.BehaviorState) + 1);
                
                if (buddy.BehaviorState == AnimalBehaviorState.ReturnToIdle) {
                    buddy.BehaviorState = AnimalBehaviorState.Idle;
                }

                if (buddy.Target == null) {
                    buddy.Target = GameRuntimeState.Player.transform;
                }

                if (buddy.BehaviorState == AnimalBehaviorState.MovingToTarget && (buddy.Transform.position - buddy.Target.position).magnitude <= buddy.ConsumingDistance + 0.1f) {
                    buddy.BehaviorState = AnimalBehaviorState.Idle;
                }

                if (buddy.BehaviorState == AnimalBehaviorState.Idle && (buddy.Transform.position - buddy.Target.position).magnitude > 15f) {
                    buddy.BehaviorState = AnimalBehaviorState.MovingToTarget;
                }
            }

            if (buddy.BehaviorState != AnimalBehaviorState.KnockDown && buddy.BehaviorState != AnimalBehaviorState.Retreat && (buddy.Transform.position - buddy.Target.position).magnitude <= ConstantsStorage.BuddyStrikeDistance) {
                buddy.LastBehaviorStateChangeTime = Time.time - ConstantsStorage.BuddyBehaviorChangePeriod + 1.1f;
                buddy.BehaviorState = AnimalBehaviorState.KnockDown;

                MainCamera.transform.DOShakePosition(0.3f, 0.6f);

                buddy.SetKnockDown();

                var soundEvent = EventBus.Get<PlaySoundEvent>();
                soundEvent.IsMusic = false;
                soundEvent.SoundId = 2;
                soundEvent.AttachedTo = null;
                soundEvent.Position = buddy.transform.position;
                EventBus.RaiseEvent(soundEvent);
            }

            if (lastState != buddy.BehaviorState) {
                if (buddy.BehaviorState == AnimalBehaviorState.Idle && (buddy.Transform.position - buddy.Target.position).magnitude <= buddy.ConsumingDistance * 2f) {
                    var skipEvent = EventBus.Get<SkipInputEvent>();
                    skipEvent.StateChanged = true;
                    skipEvent.Hide = false;
                    EventBus.RaiseEvent(skipEvent);
                } else {
                    var skipEvent = EventBus.Get<SkipInputEvent>();
                    skipEvent.StateChanged = true;
                    skipEvent.Hide = true;
                    EventBus.RaiseEvent(skipEvent);
                }
            }

            if (buddy.BehaviorState == AnimalBehaviorState.Idle) {
                var bubbleWorldPosition = buddy.transform.position + ConstantsStorage.BuddyBubbleWorldOffsetFromCharacterPivot;

                var setMessagePositionEvent = EventBus.Get<SetMessagePositionEvent>();
                setMessagePositionEvent.Position = bubbleWorldPosition;
                EventBus.RaiseEvent(setMessagePositionEvent);
            }
        }
    }
}