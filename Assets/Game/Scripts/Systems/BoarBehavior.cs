using CodePatterns.Automatization;
using CodePatterns.Events;
using CodePatterns.InjectionContainer;
using CodePatterns.ObjectPool;
using DG.Tweening;
using Scripts.Enums;
using Scripts.Events;
using Scripts.Storages;
using Scripts.Structures;
using Scripts.UnityComponents;
using UnityEngine;
using UnityEngine.Scripting;

namespace Scripts.Systems {
    [Auto(100)] [Preserve]
    public class BoarBehavior : IInjectionTarget, IEventConsumer, IEventConsumer<FixedUpdateEvent> {
        public GameStoredState GameStoredState;
        public GameRuntimeState GameRuntimeState;
        public EventBus EventBus;
        public Camera MainCamera;
        public ConstantsStorage ConstantsStorage;
        public ItemsStorage ItemsStorage;

        public ObjectPool<BoarView> BoarPool;
        public ObjectPool<EffectView> PickUpEffectPool;

        private const float StepPeriod = 2.73f;

        private float _lastStepTime;

        public void CatchEvent(FixedUpdateEvent e) {
            var player = GameRuntimeState.Player;

            while (GameRuntimeState.Boars.Count < 1 && GameStoredState.DistanceTraveled > ConstantsStorage.BoarFirstAppearedDistance) {
                var boar = BoarPool.Get();
                boar.transform.position = player.Transform.position + new Vector3(35f, 0f, 0f);
                boar.LastBehaviorStateChangeTime = Time.time - ConstantsStorage.BoarBehaviorChangePeriod;
                boar.LastEndRaidTime = Time.time - ConstantsStorage.BoarCooldown;
                GameRuntimeState.Boars.AddLast(boar);

                if (boar.Target == null) {
                    boar.Target = new GameObject() { name = "BoarTarget" }.transform;
                }
            }

            foreach (var boar in GameRuntimeState.Boars) {
                var deltaToPlayer = player.Transform.position - boar.Transform.position;
                var deltaToTarget = boar.Target.position - boar.Transform.position;

                if (deltaToTarget.magnitude < 1f) {
                    boar.Target.position -= deltaToPlayer.normalized * 15f;
                }

                if (Time.time - boar.LastBehaviorStateChangeTime > ConstantsStorage.BoarBehaviorChangePeriod
                && deltaToPlayer.magnitude > 15f) {
                    boar.LastBehaviorStateChangeTime = Time.time;

                    if (Time.time - boar.LastEndRaidTime < ConstantsStorage.BoarCooldown) {
                        boar.BehaviorState = AnimalBehaviorState.Idle;
                    } else {
                        boar.BehaviorState = AnimalBehaviorState.MovingToTarget;
                        
                        var directionAngle = Random.Range(0f, ConstantsStorage.BoarDirectionSpreadAngle * 2f + ConstantsStorage.BoarDirectionSpreadAngleStep);
                        directionAngle -= directionAngle % ConstantsStorage.BoarDirectionSpreadAngleStep;
                        directionAngle -= ConstantsStorage.BoarDirectionSpreadAngle;
                        directionAngle = directionAngle * ((Random.Range(0f, 1f) > 0.5f) ? 1f : -1f);
                        var directionRotation = Quaternion.Euler(0f, directionAngle, 0f);
                        var direction = directionRotation * Vector3.forward;

                        var offsetValue = Random.Range(0f, 3f);
                        offsetValue += -offsetValue % 1f - 1f;
                        var offset = directionRotation * Vector3.right * offsetValue;

                        boar.transform.position = player.Transform.position - direction * 15f + offset;
                        boar.Target.position = player.Transform.position + direction * 15f + offset;

                        boar.ApproachesCount++;
                        if (boar.ApproachesCount > ConstantsStorage.BoarApproachesPerRaid) {
                            boar.ApproachesCount = 0;
                            boar.LastEndRaidTime = Time.time;

                            boar.transform.position += new Vector3(100f, 0f, 0f);
                        }
                    }
                }

                if (Time.time - boar.LastRabbitsCheckTime > ConstantsStorage.BoarAffectRabbitCheckPeriod) {
                    var affectRabbitDistanceSquare = ConstantsStorage.BoarAffectRabbitDistance * ConstantsStorage.BoarAffectRabbitDistance;
                    foreach (var rabbit in GameRuntimeState.Rabbits) {
                        if ((rabbit.transform.position - boar.transform.position).sqrMagnitude < affectRabbitDistanceSquare) {
                            rabbit.BehaviorState = AnimalBehaviorState.Retreat;
                            rabbit.LastBehaviorStateChangeTime = Time.time + ConstantsStorage.BoarRabbitRetreatTime;
                            rabbit.Target = boar.transform;
                            rabbit.IsScared = true;
                            
                            if (!GameStoredState.HasItem(ItemType.Whistle)) {
                                rabbit.IsTamed = false;
                            }
                        }
                    }
                }

                foreach (var collider in boar.LatestCollisions) {
                    if (collider.TryGetComponent<EnvironmentObjectView>(out var environmentObject)) {
                        if (environmentObject) {
                            var effect = PickUpEffectPool.Get();
                            effect.transform.position = environmentObject.transform.position;
                            effect.Play();
                            GameRuntimeState.ActiveEffects.AddLast(effect);

                            environmentObject.Release();
                        }
                    } else if (collider.TryGetComponent<CharacterView>(out var character)) {
                        if (character.transform.Equals(player.Transform)) {
                            var direction = boar.transform.position - boar.Target.position;
                            var newTargetPosition = boar.transform.position + direction.normalized * 35f;
                            newTargetPosition.y = 0f;
                            boar.Target.position = newTargetPosition;

                            GameStoredState.CollectedObjects.Clear();
                            player.SetCollectedGatherables(GameStoredState.CollectedObjects);

                            foreach (var item in ItemsStorage.Items) {
                                if (item.SellPrice > 0) {
                                    var collectEvent = EventBus.Get<CollectGatherableEvent>();
                                    collectEvent.ItemType = item.TypeOfItem;
                                    collectEvent.Position = boar.transform.position;
                                    EventBus.RaiseEvent(collectEvent);
                                }
                            }

                            boar.BehaviorState = AnimalBehaviorState.MovingToTarget;

                            MainCamera.transform.DOShakePosition(0.3f, 1f);

                            var soundEvent = EventBus.Get<PlaySoundEvent>();
                            soundEvent.IsMusic = false;
                            soundEvent.SoundId = 2;
                            soundEvent.AttachedTo = null;
                            soundEvent.Position = boar.transform.position;
                            EventBus.RaiseEvent(soundEvent);

                            player.SetKnockDown();
                        }
                    }
                }
                boar.LatestCollisions.Clear();

                if (boar.Target != null && boar.BehaviorState == AnimalBehaviorState.MovingToTarget) {
                    var direction = boar.Target.position - boar.transform.position;
                    var directionNormalized = direction.normalized;
                    var velocity = direction.normalized * boar.Speed;
                    boar.transform.position += directionNormalized * Mathf.Min(velocity.magnitude * Time.fixedDeltaTime, direction.magnitude);

                    if (!Vector3.zero.Equals(directionNormalized)) {
                        var newRotation = Quaternion.LookRotation(directionNormalized, Vector3.up);
                        boar.transform.rotation = Quaternion.Slerp(boar.transform.rotation, newRotation, Time.fixedDeltaTime / ConstantsStorage.RotationSmoothApplyTime);
                    }

                    if (Time.time - _lastStepTime > StepPeriod) {
                        _lastStepTime = Time.time;
                        var soundEvent = EventBus.Get<PlaySoundEvent>();
                        soundEvent.IsMusic = false;
                        soundEvent.SoundId = 6;
                        soundEvent.AttachedTo = boar.transform;
                        soundEvent.Position = boar.transform.position;
                        EventBus.RaiseEvent(soundEvent);
                    }
                }
            }
        }
    }
}

