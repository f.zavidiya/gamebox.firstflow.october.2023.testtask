using System.Collections.Generic;
using CodePatterns.Automatization;
using CodePatterns.Events;
using CodePatterns.InjectionContainer;
using Scripts.Events;
using Scripts.Storages;
using Scripts.Structures;
using Scripts.UnityComponents;
using UnityEngine;
using UnityEngine.Scripting;

namespace Scripts.Systems {
    [Auto(100)] [Preserve]
    public class SoundSystem : IInjectionTarget, IEventConsumer, IEventConsumer<StartEvent>, IEventConsumer<FixedUpdateEvent>, IEventConsumer<SetSoundVolumeEvent>, IEventConsumer<PlaySoundEvent>, IEventConsumer<RemoveAttachedSoundsEvent> {
        public GameStoredState GameStoredState;
        public GameRuntimeState GameRuntimeState;
        public SoundsStorage SoundsStorage;
        public HUDView HUDView;

        private LinkedList<AudioSourceHolder> SourceCollection = new LinkedList<AudioSourceHolder>();
        
        public void CatchEvent(StartEvent e) {
            GameRuntimeState.AudioListener = new GameObject("AudioListener").transform;
            GameRuntimeState.AudioListener.gameObject.AddComponent<AudioListener>();
        }

        public void CatchEvent(FixedUpdateEvent e) {
            var node = SourceCollection.First;
            while (node != null) {
                var nodeNext = node.Next;
                var source = node.Value;
                if (source.AttachedTo != null) {
                    source.Source.transform.position = source.AttachedTo.position;
                }
                node = nodeNext;
            }

            GameRuntimeState.AudioListener.position = GameRuntimeState.Player.Transform.position;
        }

        public void CatchEvent(SetSoundVolumeEvent e) {
            if (e.IsMusic) {
                GameStoredState.MusicVolume = e.Volume;
            } else {
                GameStoredState.SoundVolume = e.Volume;
            }

            HUDView.SetVolume(GameStoredState.SoundVolume, GameStoredState.MusicVolume);

            var node = SourceCollection.First;
            while (node != null) {
                var nodeNext = node.Next;
                var source = node.Value;
                if (source.IsMusic == e.IsMusic) {
                    source.Source.volume = e.Volume;
                }
                node = nodeNext;
            }
        }

        public void CatchEvent(PlaySoundEvent e) {
            var node = SourceCollection.First;
            while (node != null) {
                var nodeNext = node.Next;
                var source = node.Value;
                if (source.SoundId == e.SoundId && source.IsMusic == e.IsMusic && !source.Source.isPlaying) {
                    source.Source.transform.position = e.Position;
                    source.AttachedTo = e.AttachedTo;
                    source.Source.Play();
                    return;
                }
                node = nodeNext;
            }

            var clipDescpition = SoundsStorage.GetClip(e.SoundId, e.IsMusic);

            if (clipDescpition == null) {
                return;
            }

            var newSource = new AudioSourceHolder() {
                Source = new GameObject("AudioSource").AddComponent<AudioSource>(),
                SoundId = e.SoundId,
                AttachedTo = e.AttachedTo,
                IsMusic = e.IsMusic
            };
            newSource.Source.transform.position = e.Position;
            newSource.Source.clip = clipDescpition.Clip;
            newSource.Source.volume = clipDescpition.IsMusic ? GameStoredState.MusicVolume : GameStoredState.SoundVolume;
            newSource.Source.loop = newSource.IsMusic;
            newSource.Source.spatialBlend = newSource.IsMusic ? 0f : 1f;
            newSource.Source.Play();
            SourceCollection.AddLast(newSource);
        }

        public void CatchEvent(RemoveAttachedSoundsEvent e) {
            ClearAttachedSounds(e.AttachedTo);
        }

        private void ClearAttachedSounds(Transform transform) {
            if (transform != null) {
                return;
            }

            var node = SourceCollection.First;
            while (node != null) {
                var nodeNext = node.Next;
                var source = node.Value;
                if (source.AttachedTo.Equals(transform)) {
                    SourceCollection.Remove(node);
                }
                node = nodeNext;
            }
        }
    }
}