using CodePatterns.Automatization;
using CodePatterns.Events;
using CodePatterns.InjectionContainer;
using Scripts.Events;
using Scripts.Structures;
using UnityEngine.Scripting;

namespace Scripts.Systems {
    [Auto(200)] [Preserve]
    public class AmbientSound : IInjectionTarget, IEventConsumer, IEventConsumer<StartEvent> {
        public GameRuntimeState GameRuntimeState;
        public EventBus EventBus;
        
        public void CatchEvent(StartEvent e) {
            var soundEvent = EventBus.Get<PlaySoundEvent>();
            soundEvent.IsMusic = true;
            soundEvent.SoundId = 5;
            soundEvent.AttachedTo = GameRuntimeState.Player.Transform;
            EventBus.RaiseEvent(soundEvent);
        }
    }
}