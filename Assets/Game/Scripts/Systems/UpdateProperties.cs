using CodePatterns.Automatization;
using CodePatterns.Events;
using CodePatterns.InjectionContainer;
using Scripts.Events;
using Scripts.Storages;
using Scripts.Structures;
using UnityEngine.Scripting;

namespace Scripts.Systems {
    [Auto(200)] [Preserve]
    public class UpdateProperties : IInjectionTarget, IEventConsumer, IEventConsumer<ChangePerkEvent> {
        public GameStoredState GameStoredState;
        public GameRuntimeState GameRuntimeState;
        public ConstantsStorage ConstantsStorage;

        public void CatchEvent(ChangePerkEvent e) {
            GameRuntimeState.UpdateProperties(ConstantsStorage, GameStoredState.Inventory);
        }
    }
}