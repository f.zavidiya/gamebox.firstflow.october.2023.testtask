using System;
using CodePatterns.Automatization;
using CodePatterns.Events;
using CodePatterns.InjectionContainer;
using Scripts.Events;
using Scripts.UnityComponents;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Scripting;

namespace Scripts.Systems {
    [Auto(100)] [Preserve]
    public class CatchPlayerInput : IDisposable, IInjectionTarget, IEventConsumer, IEventConsumer<StartEvent> {
        public EventBus EventBus;
        public HUDView HUDView;
        
        private PlayerInput _playerInput;
        
        public void CatchEvent(StartEvent e) {
            _playerInput = new PlayerInput();
            _playerInput.Enable();
            
            _playerInput.Game.Movement.started += OnMovement;
            _playerInput.Game.Movement.performed += OnMovement;
            _playerInput.Game.Movement.canceled += OnMovement;
            _playerInput.Game.MainMenu.started += OnMainMenu;
            _playerInput.Game.Skip.started += OnSkip;
        }

        private void OnMovement(InputAction.CallbackContext context) {
            // TODO: break dependency to UI
            if (HUDView.MainMenu.gameObject.activeSelf) {
                return;
            }

            var e = EventBus.Get<PlayerMoveInputEvent>();
            if (context.phase == InputActionPhase.Canceled || context.phase == InputActionPhase.Disabled) {
                e.Direction = Vector2.zero;
            } else {
                e.Direction = context.ReadValue<Vector2>();
            }
            EventBus.RaiseEvent(e);
        }

        private void OnMainMenu(InputAction.CallbackContext context) {
            if (context.phase == InputActionPhase.Started) {
                EventBus.RaiseEvent(EventBus.Get<OpenMainMenuEvent>());
            }
        }

        private void OnSkip(InputAction.CallbackContext context) {
            var skipEvent = EventBus.Get<SkipInputEvent>();
            skipEvent.StateChanged = false;
            skipEvent.Hide = false;
            EventBus.RaiseEvent(skipEvent);
        }

        public void Dispose() {
            if (_playerInput == null) {
                return;
            }

            _playerInput.Game.Movement.started -= OnMovement;
            _playerInput.Game.Movement.performed -= OnMovement;
            _playerInput.Game.Movement.canceled -= OnMovement;
            _playerInput.Game.MainMenu.started -= OnMainMenu;

            _playerInput.Disable();
            _playerInput = null;
        }
    }
}