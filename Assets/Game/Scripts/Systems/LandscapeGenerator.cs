using System.Collections.Generic;
using CodePatterns.Automatization;
using CodePatterns.Events;
using CodePatterns.InjectionContainer;
using CodePatterns.ObjectPool;
using Scripts.Events;
using Scripts.Storages;
using Scripts.Structures;
using Scripts.UnityComponents;
using UnityEngine;
using UnityEngine.Scripting;

namespace Scripts.Systems {
    [Auto(100)] [Preserve]
    public class LandscapeGenerator : IInjectionTarget, IEventConsumer, IEventConsumer<StartEvent>, IEventConsumer<FixedUpdateEvent> {
        public GameStoredState GameStoredState;
        public GameRuntimeState GameRuntimeState;
        public Camera MainCamera;
        public ConstantsStorage ConstantsStorage;
        public ItemsStorage ItemsStorage;
        public BiomesStorage BiomesStorage;

        public ObjectPoolCollection<string, LandscapeChunkView> GroundPoolCollection;
        public ObjectPoolCollection<string, TreeChunkView> PinePoolCollection;
        public ObjectPoolCollection<string, EnvironmentObjectView> GatherableObjectPoolCollection;
        public ObjectPool<LandscapeChunkView> RoadPool;
        public ObjectPool<SignView> SignPool;

        private Vector3 _startCameraOffset;
        private BiomeGenerationContext _biomeGeneration;

        public void CatchEvent(StartEvent e) {
            _biomeGeneration = new BiomeGenerationContext() { BiomeType = (Enums.BiomeType) int.MaxValue };

            if (GameRuntimeState.Sign == null) {
                GameRuntimeState.Sign = SignPool.Get();
                GameRuntimeState.Sign.transform.position = new Vector3(-1.5f, 0f, 2f);
                GameRuntimeState.Sign.SetValue(GameRuntimeState.Sign.transform.position.z + GameRuntimeState.GlobalOffsetZ);
            }

            _startCameraOffset = MainCamera.transform.position;
        }

        public void CatchEvent(FixedUpdateEvent e) {
            var i = 0;
            var biome = GetBiomeDescription(GameStoredState.DistanceTraveled);

            if (_biomeGeneration.BiomeType != biome.TypeOfBiome) {
                SwitchBiomeContext(_biomeGeneration, biome);
            }

            // Pre-generation
            while (GameRuntimeState.RoadChunks.Count < 5) {
                var road = RoadPool.Get();
                GameRuntimeState.RoadChunks.AddLast(road);
            }

            while (GameRuntimeState.GroundChunks.Count < 12) {
                if (GroundPoolCollection.TryGet(biome.GroundAssetPath, out var groundPool)) {
                    var ground = groundPool.Get();
                    ground.BiomeType = biome.TypeOfBiome;
                    GameRuntimeState.GroundChunks.AddLast(ground);
                }
            }

            while (GameRuntimeState.PineChunks.Count < 12) {
                if (PinePoolCollection.TryGet(biome.ChunkAssetPath, out var chunkPool)) {
                    var chunk = chunkPool.Get();
                    chunk.BiomeType = biome.TypeOfBiome;
                    GameRuntimeState.PineChunks.AddLast(chunk);
                }
            }

            var cameraAnchorPosition = MainCamera.transform.position - _startCameraOffset;

            // Generate gatherables
            if (GameRuntimeState.LastGeneratedGatherableZ < GameRuntimeState.Player.transform.position.z + 10f) {
                while (GameRuntimeState.LastGeneratedGatherableZ < GameRuntimeState.Player.transform.position.z + 10f) {
                    var generationPositionZ = GameRuntimeState.LastGeneratedGatherableZ;
                    var generationStepLength = ConstantsStorage.GatherableGenerationStep / biome.GatherablesDensityPerStep;

                    GameRuntimeState.LastGeneratedGatherableZ += ConstantsStorage.GatherableGenerationStep;
                    GameRuntimeState.AccumulatedGenerationDistance += ConstantsStorage.GatherableGenerationStep;
                    
                    while (GameRuntimeState.AccumulatedGenerationDistance > generationStepLength) {
                        GameRuntimeState.AccumulatedGenerationDistance -= generationStepLength;
                        generationPositionZ += generationStepLength;

                        var randomValue = Random.Range(0f, _biomeGeneration.TotalItemsGenerationChance);
                        var agregatedChance = 0f;
                        var count = ItemsStorage.Items.Count;

                        var item = ItemsStorage.Items[0];
                        for (i = 0; i < count; i++) {
                            if (_biomeGeneration.BiomeIndex < ItemsStorage.Items[i].BiomeSpawnChance.Count) {
                                agregatedChance += ItemsStorage.Items[i].BiomeSpawnChance[_biomeGeneration.BiomeIndex];
                                if (agregatedChance >= randomValue) {
                                    item = ItemsStorage.Items[i];
                                    i = count;
                                }
                            }
                        }

                        if (GatherableObjectPoolCollection.TryGet(item.AssetPath, out var pool)) {
                            var gatherable = pool.Get();
                            gatherable.ItemType = item.TypeOfItem;
                            gatherable.transform.position = new Vector3(Random.Range(2f, 30f) * (Random.Range(0f, 1f) > 0.5f ? 1f : -1f), 0f, generationPositionZ);
                            GameRuntimeState.Gatherables.AddLast(gatherable);
                            GameRuntimeState.GatherablesByHash.TryAdd(gatherable.gameObject.GetHashCode(), gatherable);
                        } else {
                            break;
                        }
                    }
                }
            }

            // Clear gatherables out of view
            var gatherableMinPositionZ = GameRuntimeState.Player.transform.position.z - 10f;
            var node = GameRuntimeState.Gatherables.First;
            while (node != null) {
                var nodeNext = node.Next;
                if (node.Value.transform.position.z < gatherableMinPositionZ) {
                    GameRuntimeState.GatherablesByHash.Remove(node.Value.gameObject.GetHashCode());
                    node.Value.Release();
                    GameRuntimeState.Gatherables.Remove(node);
                    node = nodeNext;
                } else {
                    node = null;
                }
            }

            // Move road patches
            i = 0;
            foreach (var roadTile in GameRuntimeState.RoadChunks) {
                var position = new Vector3(0f, 0f, (i - 0.8f) * 5f + cameraAnchorPosition.z + 100f);
                position.z -= position.z % 5f + 100f;
                roadTile.transform.position = position;
                i++;
            }

            // Move / generate ground patches
            i = 0;
            var landscapeChunkNode = GameRuntimeState.GroundChunks.First;
            while (landscapeChunkNode != null) {
                var nodeNext = landscapeChunkNode.Next;
                var chunk = landscapeChunkNode.Value;

                var position = GetChunkPosition(i, cameraAnchorPosition);

                if ((chunk.transform.position - position).sqrMagnitude > 1f) {
                    chunk.transform.position = position;
                    TryReplaceChunk(landscapeChunkNode, GroundPoolCollection);
                }
                i++;

                landscapeChunkNode = nodeNext;
            }

            // Move / generate vegetable chunks
            i = 0;
            var pineChunkNode = GameRuntimeState.PineChunks.First;
            while (pineChunkNode != null) {
                var nodeNext = pineChunkNode.Next;
                var chunk = pineChunkNode.Value;

                var position = GetChunkPosition(i, cameraAnchorPosition);

                if ((chunk.transform.position - position).sqrMagnitude > 1f) {
                    chunk.transform.position = position;
                    chunk.Randomize(GameStoredState.WorldSeed + (int) (position.x * 10f) + (int) ((position.z + GameRuntimeState.GlobalOffsetZ) * 10000f), ConstantsStorage.RandomizationWeights);

                    if (TryReplaceChunk(pineChunkNode, PinePoolCollection)) {
                        chunk = pineChunkNode.Value;
                        chunk.Randomize(GameStoredState.WorldSeed + (int) (position.x * 10f) + (int) ((position.z + GameRuntimeState.GlobalOffsetZ) * 10000f), ConstantsStorage.RandomizationWeights);
                    }
                }
                i++;

                pineChunkNode = nodeNext;
            }
        }

        private void SwitchBiomeContext(BiomeGenerationContext context, BiomesStorage.Biome reference) {
            context.BiomeType = reference.TypeOfBiome;
            context.BiomeIndex = (int) context.BiomeType;
            context.TotalItemsGenerationChance = 0f;
            var biomeIndex = (int) context.BiomeType;
            foreach (var item in ItemsStorage.Items) {
                if (biomeIndex < item.BiomeSpawnChance.Count) {
                    context.TotalItemsGenerationChance += item.BiomeSpawnChance[biomeIndex];
                }
            }
        }

        private Vector3 GetChunkPosition(int chunkIndex, Vector3 anchorPosition) {
            var x = (chunkIndex % 4 - 1) * 10f + anchorPosition.x + 100f;
            x -= x % 10f;
            var z = ((chunkIndex - chunkIndex % 4) / 4f - 0.25f) * 10f + anchorPosition.z + 100f;
            z -= z % 10f;
            return new Vector3(x - 100f, 0f, z - 100f);
        }

        private BiomesStorage.Biome GetBiomeDescription(float distance) {
            var lastBiome = BiomesStorage.Biomes[BiomesStorage.Biomes.Count - 1];
            for (var i = BiomesStorage.Biomes.Count - 2; i >= 0; i--) {
                var biome = BiomesStorage.Biomes[i];
                if (distance < biome.EndDistance) {
                    lastBiome = biome;
                } else {
                    break;
                }
            }
            return lastBiome;
        }

        private bool TryReplaceChunk<T>(LinkedListNode<T> node, ObjectPoolCollection<string, T> poolCollection) where T : LandscapeChunkView {
            var chunk = (LandscapeChunkView) node.Value;
            var chunkBiome = GetBiomeDescription(GameRuntimeState.GlobalOffsetZ + chunk.transform.position.z);
            if (chunkBiome.TypeOfBiome != chunk.BiomeType) {
                var storedChunkPosition = chunk.transform.position;
                var assetPath = (chunk is TreeChunkView) ? chunkBiome.ChunkAssetPath : chunkBiome.GroundAssetPath;
                if (poolCollection.TryGet(assetPath, out var chunkPool)) {
                    chunk = chunkPool.Get();
                    chunk.transform.position = storedChunkPosition;
                    chunk.BiomeType = chunkBiome.TypeOfBiome;
                    node.Value.Release();
                    node.Value = (T) chunk;
                    return true;
                }
            }
            return false;
        }
    }
}