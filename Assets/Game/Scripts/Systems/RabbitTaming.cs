using CodePatterns.Automatization;
using CodePatterns.Events;
using CodePatterns.InjectionContainer;
using Scripts.Enums;
using Scripts.Events;
using Scripts.Structures;
using Scripts.UnityComponents;
using UnityEngine;
using UnityEngine.Scripting;

namespace Scripts.Systems {
    [Auto(99)] [Preserve]
    public class RabbitTaming : IInjectionTarget, IEventConsumer, IEventConsumer<FixedUpdateEvent> {
        public GameStoredState GameStoredState;
        public GameRuntimeState GameRuntimeState;
        public EventBus EventBus;
        public Camera MainCamera;

        public void CatchEvent(FixedUpdateEvent e) {
            foreach (var collider in GameRuntimeState.Player.LatestCollisions) {
                if (collider.transform.root.TryGetComponent<AnimalView>(out var animal)) {
                    if (!animal.IsTamed && GameStoredState.HasItem(ItemType.Carrot)) {
                        GameStoredState.RemoveItem(ItemType.Carrot, 1);

                        var changePerkEvent = EventBus.Get<ChangePerkEvent>();
                        changePerkEvent.ItemType = ItemType.Carrot;
                        changePerkEvent.Position = animal.transform.position;
                        EventBus.RaiseEvent(changePerkEvent);

                        animal.IsTamed = true;

                        if (animal.IsScared) {
                            animal.IsScared = false;
                        }
                    }
                }
            }
        }
    }
}