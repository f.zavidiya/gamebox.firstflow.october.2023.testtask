using CodePatterns.Automatization;
using CodePatterns.Events;
using CodePatterns.InjectionContainer;
using CodePatterns.ObjectPool;
using Scripts.Enums;
using Scripts.Events;
using Scripts.Structures;
using Scripts.UnityComponents;
using UnityEngine;
using UnityEngine.Scripting;

namespace Scripts.Systems {
    [Auto(100)] [Preserve]
    public class GatherObjects : IInjectionTarget, IEventConsumer, IEventConsumer<FixedUpdateEvent> {
        public GameStoredState GameStoredState;
        public GameRuntimeState GameRuntimeState;
        public EventBus EventBus;
        public Camera MainCamera;

        public ObjectPool<EffectView> PickUpEffectPool;

        public void CatchEvent(FixedUpdateEvent e) {
            var unit = GameRuntimeState.Player;

            if (unit.IsFull) {
                ClearGatherableCollisions(unit);
                return;
            }

            CheckGatheringObjectNotInActiveCollisions(unit);
            SetGatheringEarlierActiveCollisionObject(unit);
            FinishGathering(unit);
        }

        private void ClearGatherableCollisions(CharacterView unit) {
            if (unit.LatestCollisions.Count > 0) {
                var hasCollision = false;
                var node = unit.LatestCollisions.First;
                while (node != null) {
                    var nodeNext = node.Next;
                    var collider = node.Value;
                    var hash = collider.gameObject.GetHashCode();
                    if (GameRuntimeState.GatherablesByHash.TryGetValue(hash, out var environmentObject)) {
                        unit.LatestCollisions.Remove(collider);
                        hasCollision = true;
                    }
                    node = nodeNext;
                }
                if (hasCollision) {
                    EventBus.RaiseEvent(EventBus.Get<BasketBlockedEvent>());
                }
            }
        }

        private void CheckGatheringObjectNotInActiveCollisions(CharacterView unit) {
            if (unit.GatheringTarget != null) {
                if (!unit.LatestCollisions.Contains(unit.GatheringTarget.TriggerCollider)) {
                    unit.GatheringTarget = null;
                }
            }
        }

        private void SetGatheringEarlierActiveCollisionObject(CharacterView unit) {
            if (unit.GatheringTarget != null) {
                return;
            }

            var node = unit.LatestCollisions.First;
            while (node != null) {
                var nodeNext = node.Next;
                var collider = node.Value;
                var hash = collider.gameObject.GetHashCode();
                if (GameRuntimeState.GatherablesByHash.TryGetValue(hash, out var environmentObject)) {
                    var effect = PickUpEffectPool.Get();
                    effect.transform.position = environmentObject.transform.position;
                    effect.SetLifetime(unit.GatheringTime);
                    effect.Play();
                    GameRuntimeState.ActiveEffects.AddLast(effect);

                    unit.GatheringTarget = environmentObject;
                    unit.GatheringStartTime = Time.time;
                }
                node = nodeNext;
            }
        }

        private void FinishGathering(CharacterView unit) {
            if (unit.GatheringTarget != null && Time.time - unit.GatheringStartTime > unit.GatheringTime) {
                var environmentObject = unit.GatheringTarget;
                var hash = environmentObject.gameObject.GetHashCode();

                environmentObject.Release();
                GameRuntimeState.GatherablesByHash.Remove(hash);
                GameRuntimeState.Gatherables.Remove(environmentObject);

                GameStoredState.AddCollectedObject(environmentObject.ItemType, 1);
                unit.SetCollectedGatherables(GameStoredState.CollectedObjects);

                var collectEvent = EventBus.Get<CollectGatherableEvent>();
                collectEvent.ItemType = environmentObject.ItemType;
                collectEvent.Position = environmentObject.transform.position;
                EventBus.RaiseEvent(collectEvent);

                if (GameStoredState.HasItem(ItemType.Gloves)) {
                    GameStoredState.RemoveItem(ItemType.Gloves, 1);

                    var changePerkEvent = EventBus.Get<ChangePerkEvent>();
                    changePerkEvent.ItemType = ItemType.Gloves;
                    changePerkEvent.Position = Vector3.zero;
                    EventBus.RaiseEvent(changePerkEvent);
                }

                var soundEvent = EventBus.Get<PlaySoundEvent>();
                soundEvent.IsMusic = false;
                soundEvent.SoundId = 1;
                soundEvent.AttachedTo = null;
                soundEvent.Position = unit.GatheringTarget.transform.position;
                EventBus.RaiseEvent(soundEvent);

                unit.GatheringTarget = null;

                unit.LatestCollisions.Remove(environmentObject.TriggerCollider);
            }
        }
    }
}