using System.IO;
using CodePatterns.Automatization;
using CodePatterns.Events;
using CodePatterns.InjectionContainer;
using CodePatterns.Serialization;
using Scripts.Events;
using Scripts.Storages;
using Scripts.Structures;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Scripting;

namespace Scripts.Systems {
    [Auto(10)] [Preserve]
    public class SaveLoad : IInjectionTarget, IEventConsumer, IEventConsumer<StartEvent>, IEventConsumer<UpdateEvent>, IEventConsumer<SaveEvent>, IEventConsumer<LoadEvent>, IEventConsumer<NewGameEvent> {
        public GameStoredState GameStoredState;
        public GameRuntimeState GameRuntimeState;
        public EventBus EventBus;
        public ConstantsStorage ConstantsStorage;
        public ItemsStorage ItemsStorage;

        private ByteBuffer _byteBuffer = new ByteBuffer();
        private string _savePath;

        public void CatchEvent(StartEvent e) {
            _savePath = $"{Application.persistentDataPath}/current.save";

            _byteBuffer = new ByteBuffer() { Pointer = 0 };

            Load();
        }

        public void CatchEvent(UpdateEvent e) {
            if (Time.time - GameRuntimeState.LastSaveTime > ConstantsStorage.SaveTimePeriod) {
                Save();
            }
        }

        public void CatchEvent(SaveEvent currentEvent) {
            Save();
        }

        public void CatchEvent(LoadEvent currentEvent) {
            Load();
        }

        public void CatchEvent(NewGameEvent currentEvent) {
            ClearProgressAndRestart();
        }

        private void Load() {
            GameRuntimeState.LastSaveTime = Time.time;

            if (File.Exists(_savePath)) {
                _byteBuffer.Data = File.ReadAllBytes(_savePath);
                _byteBuffer.Pointer = 0;
                GameStoredState.Unpack(_byteBuffer);
            }

            GameRuntimeState.GlobalOffsetZ = GameStoredState.DistanceTraveled - GameStoredState.DistanceTraveled % 100f;

            GameRuntimeState.UpdateProperties(ConstantsStorage, GameStoredState.Inventory);

            EventBus.RaiseEvent(EventBus.Get<ChangeCashEvent>());
            foreach (var item in ItemsStorage.Items) {
                if (item.SellPrice <= 0) {
                    var changePerkEvent = EventBus.Get<ChangePerkEvent>();
                    changePerkEvent.ItemType = item.TypeOfItem;
                    changePerkEvent.Position = Vector3.zero;
                    EventBus.RaiseEvent(changePerkEvent);
                } else {
                    var collectEvent = EventBus.Get<CollectGatherableEvent>();
                    collectEvent.ItemType = item.TypeOfItem;
                    collectEvent.Position = Vector3.zero;
                    EventBus.RaiseEvent(collectEvent);
                }
            }

            var setVolumeEvent = EventBus.Get<SetSoundVolumeEvent>();
            setVolumeEvent.IsMusic = true;
            setVolumeEvent.Volume = GameStoredState.MusicVolume;
            EventBus.RaiseEvent(setVolumeEvent);

            setVolumeEvent = EventBus.Get<SetSoundVolumeEvent>();
            setVolumeEvent.IsMusic = false;
            setVolumeEvent.Volume = GameStoredState.SoundVolume;
            EventBus.RaiseEvent(setVolumeEvent);

#if UNITY_EDITOR
            Debug.Log($"SAVE UNPACKED | PlayerName: {GameStoredState.PlayerName}; Cash: {GameStoredState.Cash}; WorldSeed: {GameStoredState.WorldSeed}; TotalGameTime: {GameStoredState.TotalGameTime}; DistanceTraveled: {GameStoredState.DistanceTraveled}; GlobalOffsetZ: {GameRuntimeState.GlobalOffsetZ}; PlayerPosition: {GameStoredState.PlayerPosition}");
            Debug.Log($"PERKS:");
            foreach (var perk in GameStoredState.Perks) {
                Debug.Log($" - {perk.Type} ({perk.Level})");
            }
            Debug.Log($"ITEMS:");
            foreach (var item in GameStoredState.Inventory) {
                Debug.Log($" - {item.Type} ({item.Amount})");
            }
            Debug.Log($"COLLECTED:");
            foreach (var keyValue in GameStoredState.CollectedObjects) {
                Debug.Log($" - {keyValue.Key} ({keyValue.Value})");
            }
#endif
        }

        private void Save() {
            GameStoredState.TotalGameTime += Time.time - GameRuntimeState.LastSaveTime;
            GameRuntimeState.LastSaveTime = Time.time;
            GameStoredState.DistanceTraveled = GameRuntimeState.GlobalOffsetZ + GameRuntimeState.Player.transform.position.z;

            var dataLength = GameStoredState.GetStructureLength();

            if (_byteBuffer.Data == null || _byteBuffer.Data.Length < dataLength) {
                _byteBuffer.Data = new byte[dataLength * 2];
            }

            _byteBuffer.Pointer = 0;

            GameStoredState.Pack(_byteBuffer);

            File.WriteAllBytes(_savePath, _byteBuffer.Data);
        }

        private void ClearProgressAndRestart() {
            if (File.Exists(_savePath)) {
                File.Delete(_savePath);
            }
            SceneManager.LoadScene(0);
        }
    }
}