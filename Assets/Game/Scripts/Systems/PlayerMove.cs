using CodePatterns.Automatization;
using CodePatterns.Events;
using CodePatterns.InjectionContainer;
using Scripts.Events;
using Scripts.Storages;
using Scripts.Structures;
using UnityEngine;
using UnityEngine.Scripting;

namespace Scripts.Systems {
    [Auto(100)] [Preserve]
    public class PlayerMove : IInjectionTarget, IEventConsumer, IEventConsumer<UpdateEvent>, IEventConsumer<FixedUpdateEvent>, IEventConsumer<PlayerMoveInputEvent> {
        public EventBus EventBus;
        public GameStoredState GameStoredState;
        public GameRuntimeState GameRuntimeState;
        public ConstantsStorage ConstantsStorage;

        private const float StepPeriod = 0.33f;

        private float _lastStepTime;
        
        public void CatchEvent(UpdateEvent e) {
            var player = GameRuntimeState.Player;

            player.SetVelocity(player.Rigidbody.velocity);

            if (player.Direction.sqrMagnitude > float.Epsilon) {
                var newRotation = Quaternion.LookRotation(new Vector3(player.Direction.x, 0f, player.Direction.y), Vector3.up);
                player.transform.rotation = Quaternion.Slerp(player.transform.rotation, newRotation, Time.fixedDeltaTime / ConstantsStorage.RotationSmoothApplyTime);
            }
        }

        public void CatchEvent(FixedUpdateEvent e) {
            var player = GameRuntimeState.Player;
            var newVelocity = new Vector3(player.Direction.x, 0f, player.Direction.y) * player.Speed;
            newVelocity = Vector3.Lerp(player.Rigidbody.velocity, newVelocity, Time.fixedDeltaTime / ConstantsStorage.VelocitySmoothApplyTime);
            newVelocity.y = player.Rigidbody.velocity.y;

            if (player.IsKnockedDown) {
                newVelocity *= 0f;
            }

            var backEdgeZ = GameRuntimeState.LastGeneratedGatherableZ + ConstantsStorage.BackEdgeOffset;
            if (newVelocity.z < 0f && player.transform.position.z + newVelocity.z * 0.5f < backEdgeZ) {
                newVelocity.z = Mathf.Max(newVelocity.z, Mathf.Min((backEdgeZ - player.transform.position.z) / 0.5f, 0f));
            }

            player.Rigidbody.velocity = newVelocity;
            player.Rigidbody.angularVelocity *= 1f - Time.fixedDeltaTime * 5f;

            if (player.Rigidbody.velocity.sqrMagnitude > 1f && Time.time - _lastStepTime > StepPeriod) {
                _lastStepTime = Time.time;

                var soundEvent = EventBus.Get<PlaySoundEvent>();
                soundEvent.IsMusic = false;
                soundEvent.SoundId = 4;
                soundEvent.AttachedTo = null;
                soundEvent.Position = player.Transform.position;
                EventBus.RaiseEvent(soundEvent);
            }
        }

        public void CatchEvent(PlayerMoveInputEvent e) {
            var player = GameRuntimeState.Player;
            player.Direction = e.Direction;
        }
    }
}