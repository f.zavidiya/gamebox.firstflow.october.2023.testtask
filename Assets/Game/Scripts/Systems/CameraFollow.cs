using CodePatterns.Automatization;
using CodePatterns.Events;
using CodePatterns.InjectionContainer;
using Scripts.Events;
using Scripts.Storages;
using Scripts.Structures;
using UnityEngine;
using UnityEngine.Scripting;

namespace Scripts.Systems {
    [Auto(100)] [Preserve]
    public class CameraFollow : IInjectionTarget, IEventConsumer, IEventConsumer<StartEvent>, IEventConsumer<UpdateEvent> {
        public GameRuntimeState GameRuntimeState;
        public Camera MainCamera;
        public ConstantsStorage ConstantsStorage;

        private Vector3 CameraOffset;

        public void CatchEvent(StartEvent e) {
            CameraOffset = MainCamera.transform.position;
        }
        
        public void CatchEvent(UpdateEvent e) {
            var player = GameRuntimeState.Player;

            var newCameraPosition = player.transform.position + player.Rigidbody.velocity * ConstantsStorage.CameraVelocityShiftMultiplier + CameraOffset;

            MainCamera.transform.position = Vector3.Slerp(MainCamera.transform.position, newCameraPosition, Time.fixedDeltaTime / ConstantsStorage.CameraPositionSmoothApplyTime);
        }
    }
}