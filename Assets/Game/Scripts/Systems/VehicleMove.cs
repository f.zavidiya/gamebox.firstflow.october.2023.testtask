using CodePatterns.Automatization;
using CodePatterns.Events;
using CodePatterns.InjectionContainer;
using CodePatterns.ObjectPool;
using Scripts.Events;
using Scripts.Storages;
using Scripts.Structures;
using Scripts.UnityComponents;
using UnityEngine;
using UnityEngine.Scripting;

namespace Scripts.Systems {
    [Auto(100)] [Preserve]
    public class VehicleMove : IInjectionTarget, IEventConsumer, IEventConsumer<FixedUpdateEvent> {
        public EventBus EventBus;
        public GameRuntimeState GameRuntimeState;
        public ConstantsStorage ConstantsStorage;
        
        public ObjectPool<VehicleView> CarPool;
        
        public void CatchEvent(FixedUpdateEvent e) {
            while (GameRuntimeState.VehicleQueue.Count < GameRuntimeState.CarsCount) {
                var car = CarPool.Get();
                var position = GameRuntimeState.Player.transform.position;
                position.x = 0;
                position.y = 0;
                position.z -= 6f;
                car.transform.position = position;
                GameRuntimeState.VehicleQueue.AddLast(car);
                car.SetActive(true);
            }

            var i = 0;
            var node = GameRuntimeState.VehicleQueue.First;
            while (node != null) {
                var nodeNext = node.Next;
                var vehicle = node.Value;
                if (vehicle.IsActive) {
                    var targetZ = GameRuntimeState.LastGeneratedGatherableZ + ConstantsStorage.VehicleTargetPositionZOffset + i * ConstantsStorage.VehicleQueueOffset;
                    if (vehicle.transform.position.z < targetZ) {
                        var delta = targetZ - vehicle.transform.position.z;
                        var offscreenSpeedBoost = Mathf.Max(delta - 3f, 0f);
                        vehicle.transform.position += new Vector3(0f, 0f, 
                            Mathf.Min(ConstantsStorage.VehicleSpeed + offscreenSpeedBoost, targetZ - vehicle.transform.position.z)) * Time.fixedDeltaTime;
                    }
                    
                    if (Time.time - vehicle.LastEngineSoundTime > 1f) {
                        vehicle.LastEngineSoundTime = Time.time;
                        
                        var soundEvent = EventBus.Get<PlaySoundEvent>();
                        soundEvent.IsMusic = false;
                        soundEvent.SoundId = 7;
                        soundEvent.AttachedTo = vehicle.transform;
                        soundEvent.Position = vehicle.transform.position;
                        EventBus.RaiseEvent(soundEvent);
                    }
                }
                i++;
                node = nodeNext;
            }
        }
    }
}