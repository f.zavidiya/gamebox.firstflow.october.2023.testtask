using CodePatterns.Automatization;
using CodePatterns.AutoWrappers;
using CodePatterns.ObjectPool;
using Scripts.Events;
using Scripts.Storages;
using Scripts.Structures;
using Scripts.UnityComponents;
using UnityEngine;

namespace Scripts {
    public class Startup : MonoBehaviour {
        public Camera MainCamera;
        public Light DirectionalLight;
        public HUDView HUDView;
        public ConstantsStorage ConstantsStorage;
        public ItemsStorage ItemsStorage;
        public MessagesStorage MessagesStorage;
        public SoundsStorage SoundsStorage;
        public BiomesStorage BiomesStorage;

        private GameStoredState _gameStoredState;
        private GameRuntimeState _gameRuntimeState;
        private Automator _automator;
        private AutoInjectionContainer _injectionContainer;
        private AutoEventBus _eventBus;
        private ObjectPool<CharacterView> _playerCharacterPool = new ObjectPool<CharacterView>("Prefabs/Characters/PlayerCharacter");
        private ObjectPool<AIView> _buddyCharacterPool = new ObjectPool<AIView>("Prefabs/Characters/BuddyCharacter");
        private ObjectPool<AnimalView> _rabbitPool = new ObjectPool<AnimalView>("Prefabs/Characters/Rabbit");
        private ObjectPool<BoarView> _boarPool = new ObjectPool<BoarView>("Prefabs/Characters/Boar");
        private ObjectPool<VehicleView> _carPool = new ObjectPool<VehicleView>("Prefabs/Vehicles/Car");
        private ObjectPool<LandscapeChunkView> _roadPool = new ObjectPool<LandscapeChunkView>("Prefabs/Environment/Landscape/Road");
        private ObjectPoolCollection<string, LandscapeChunkView> _groundPoolCollection = new ObjectPoolCollection<string, LandscapeChunkView>("Prefabs/{0}");
        private ObjectPoolCollection<string, TreeChunkView> _pinePoolCollection = new ObjectPoolCollection<string, TreeChunkView>("Prefabs/{0}");
        private ObjectPoolCollection<string, EnvironmentObjectView> _gatherableObjectPoolCollection = new ObjectPoolCollection<string, EnvironmentObjectView>("Prefabs/{0}");
        private ObjectPool<EffectView> _pickUpEffectPool = new ObjectPool<EffectView>("Prefabs/Effects/PickUpSplash");
        private ObjectPool<WorldMarkerIconView> _uiVehicleMarkerPool = new ObjectPool<WorldMarkerIconView>("Prefabs/UI/VehicleMarker");
        private ObjectPool<ShopView> _shopPool = new ObjectPool<ShopView>("Prefabs/Environment/Objects/Shop");
        private ObjectPool<SignView> _signPool = new ObjectPool<SignView>("Prefabs/Environment/Objects/Sign");

        public void Start() {
            Application.targetFrameRate = 60;
            
            _injectionContainer = new AutoInjectionContainer();
            _eventBus = new AutoEventBus();
            _automator = new Automator();
            _automator.Register(_injectionContainer);
            _automator.Register(_eventBus);

            HUDView.EventBus = _eventBus;
            HUDView.MainCamera = MainCamera;

            _gameStoredState = new GameStoredState() {
                PlayerName = "Mushroomer",
                WorldSeed = 100500
            };

            _gameRuntimeState = new GameRuntimeState();

            _injectionContainer
                .RegisterInjection(_eventBus, "EventBus")
                .RegisterInjection(_gameStoredState, "GameStoredState")
                .RegisterInjection(_gameRuntimeState, "GameRuntimeState")
                .RegisterInjection(ConstantsStorage, "ConstantsStorage")
                .RegisterInjection(ItemsStorage, "ItemsStorage")
                .RegisterInjection(MessagesStorage, "MessagesStorage")
                .RegisterInjection(SoundsStorage, "SoundsStorage")
                .RegisterInjection(BiomesStorage, "BiomesStorage")
                .RegisterInjection(_playerCharacterPool, "PlayerCharacterPool")
                .RegisterInjection(_buddyCharacterPool, "BuddyCharacterPool")
                .RegisterInjection(_rabbitPool, "RabbitPool")
                .RegisterInjection(_boarPool, "BoarPool")
                .RegisterInjection(_carPool, "CarPool")
                .RegisterInjection(_shopPool, "ShopPool")
                .RegisterInjection(_signPool, "SignPool")
                .RegisterInjection(_roadPool, "RoadPool")
                .RegisterInjection(_groundPoolCollection, "GroundPoolCollection")
                .RegisterInjection(_pinePoolCollection, "PinePoolCollection")
                .RegisterInjection(_gatherableObjectPoolCollection, "GatherableObjectPoolCollection")
                .RegisterInjection(_pickUpEffectPool, "PickUpEffectPool")
                .RegisterInjection(_uiVehicleMarkerPool, "UIVehicleMarkerPool")
                .RegisterInjection(MainCamera, "MainCamera")
                .RegisterInjection(HUDView, "HUDView");

            // Auto-registered systems located only in /Assets/Game/Scripts/Systems/
            _automator.Init();
            _automator.Start();

            _eventBus.RaiseEvent(_eventBus.Get<StartEvent>());
        }

        public void Update() {
            _eventBus.RunDeferredEvents();
            _eventBus.RaiseEvent(_eventBus.Get<UpdateEvent>());
        }

        public void FixedUpdate() {
            _eventBus.RaiseEvent(_eventBus.Get<FixedUpdateEvent>());
        }
    }
}