using Scripts.Enums;

namespace Scripts.Structures {
    public class BiomeGenerationContext {
        public BiomeType BiomeType;
        public int BiomeIndex;
        public float TotalItemsGenerationChance;
    }
}