using CodePatterns.Serialization;
using Scripts.Enums;

namespace Scripts.Structures {
    public class Perk : ISerializable {
        public PerkType Type;
        public int Level;

        private const int VERSION = 1;

        public void Pack(ByteBuffer buffer) {
            SerializationUtils.Put(buffer, VERSION);
            SerializationUtils.Put(buffer, (int) Type);
            SerializationUtils.Put(buffer, Level);
        }

        public void Unpack(ByteBuffer buffer) {
            var version = SerializationUtils.Get(buffer, 0);
            Type = (PerkType) SerializationUtils.Get(buffer, 0);
            Level = SerializationUtils.Get(buffer, 0);
        }

        public int GetStructureLength() {
            return SerializationUtils.SIZE_OF_INT * 3;
        }
    }
}