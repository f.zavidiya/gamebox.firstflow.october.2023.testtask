using Scripts.Enums;
using CodePatterns.Serialization;

namespace Scripts.Structures {
    public class InventoryItem : ISerializable {
        public ItemType Type;
        public int Amount;

        private const int VERSION = 1;

        public void Pack(ByteBuffer buffer) {
            SerializationUtils.Put(buffer, VERSION);
            SerializationUtils.Put(buffer, (int) Type);
            SerializationUtils.Put(buffer, Amount);
        }

        public void Unpack(ByteBuffer buffer) {
            var version = SerializationUtils.Get(buffer, 0);
            Type = (ItemType) SerializationUtils.Get(buffer, 0);
            Amount = SerializationUtils.Get(buffer, 0);
        }

        public int GetStructureLength() {
            return SerializationUtils.SIZE_OF_INT * 3;
        }
    }
}