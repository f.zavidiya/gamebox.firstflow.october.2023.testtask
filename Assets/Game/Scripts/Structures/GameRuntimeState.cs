using System.Collections.Generic;
using Scripts.Enums;
using Scripts.Storages;
using Scripts.UnityComponents;
using UnityEngine;

namespace Scripts.Structures {
    public class GameRuntimeState {

#region Properties

        public int CarsCount { get; private set; } = 1;
        public bool PetsRecover { get; private set; } = false;
        public float PetsSpeedMultiplier { get; private set; } = 1.0f;

#endregion

#region InternalFields

        public CharacterView Player;
        public AIView Buddy;
        public ShopView Shop;
        public SignView Sign;
        public WorldMarkerIconView UIVehicleIcon;
        public Transform AudioListener;

        public float GlobalOffsetZ;
        public float LastSaveTime;
        public float LastGeneratedGatherableZ;
        public float AccumulatedGenerationDistance;

        public LinkedList<LandscapeChunkView> GroundChunks = new LinkedList<LandscapeChunkView>();
        public LinkedList<LandscapeChunkView> RoadChunks = new LinkedList<LandscapeChunkView>();
        public LinkedList<TreeChunkView> PineChunks = new LinkedList<TreeChunkView>();
        public LinkedList<EnvironmentObjectView> Gatherables = new LinkedList<EnvironmentObjectView>();
        public Dictionary<int, EnvironmentObjectView> GatherablesByHash = new Dictionary<int, EnvironmentObjectView>();
        public LinkedList<VehicleView> VehicleQueue = new LinkedList<VehicleView>();
        public LinkedList<AnimalView> Rabbits = new LinkedList<AnimalView>();
        public LinkedList<BoarView> Boars = new LinkedList<BoarView>();
        public LinkedList<EffectView> ActiveEffects = new LinkedList<EffectView>();

#endregion

#region Methods

        public void UpdateProperties(ConstantsStorage constantStorage, List<InventoryItem> inventory) {
            CarsCount = 1;
            Player.Speed = constantStorage.PlayerSpeed;
            Player.BasketCapacity = constantStorage.PlayerBasketCapacity;
            ((SphereCollider) Player.TriggerCollider).radius = constantStorage.PlayerGatheringRadius;
            Player.GatheringTime = constantStorage.PlayerGatheringTime;
            PetsRecover = false;
            PetsSpeedMultiplier = 1.0f;
            foreach (var item in inventory) {
                switch (item.Type) {
                    case ItemType.Car:
                        CarsCount += item.Amount;
                    break;
                    case ItemType.Boots:
                        Player.Speed *= constantStorage.BootsSpeedMultiplier;
                    break;
                    case ItemType.BigBasket:
                        Player.BasketCapacity *= constantStorage.BigBasketCapacityMultiplier;
                    break;
                    case ItemType.Stick:
                        ((SphereCollider) Player.TriggerCollider).radius = constantStorage.StickGatheringRadius;
                    break;
                    case ItemType.Whistle:
                        PetsRecover = true;
                    break;
                    case ItemType.Drum:
                        PetsSpeedMultiplier = constantStorage.DrumSpeedMultiplier;
                    break;
                    case ItemType.Gloves:
                        Player.GatheringTime = constantStorage.GlovesGatheringTime;
                    break;
                }
            }
        }

#endregion

    }
}