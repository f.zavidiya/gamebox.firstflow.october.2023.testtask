using UnityEngine;

namespace Scripts.Structures {
    public class AudioSourceHolder {
        public bool IsMusic;
        public int SoundId;
        public AudioSource Source;
        public Transform AttachedTo;
    }
}