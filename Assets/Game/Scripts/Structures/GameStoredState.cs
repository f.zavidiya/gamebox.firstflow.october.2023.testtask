using System.Collections.Generic;
using CodePatterns.Serialization;
using Scripts.Enums;
using Scripts.Serializators;
using UnityEngine;

namespace Scripts.Structures {
    public class GameStoredState : ISerializable {
        private const int VERSION = 1; // Version of component serialization scheme

#region SerializableFields
        
        public string PlayerName;
        public int WorldSeed;
        public float TotalGameTime;
        public float DistanceTraveled;
        public int Cash;
        public Vector3 PlayerPosition;
        public List<Perk> Perks = new List<Perk>();
        public List<InventoryItem> Inventory = new List<InventoryItem>();
        public Dictionary<ItemType, int> CollectedObjects = new Dictionary<ItemType, int>();
        public int TutorialState;
        public float SoundVolume = 1f;
        public float MusicVolume = 1f;

#endregion

#region Methods

        public void AddCollectedObject(ItemType type, int amount) {
            if (CollectedObjects.TryGetValue(type, out var lastAmount)) {
                CollectedObjects[type] = lastAmount + amount;
                return;
            }
            CollectedObjects.TryAdd(type, amount);
        }

        public int GetCollectedObjectAmount(ItemType type) {
            if (CollectedObjects.TryGetValue(type, out var amount)) {
                return amount;
            }
            return 0;
        }

        public void AddItem(ItemType type, int amount) {
            foreach (var item in Inventory) {
                if (item.Type == type) {
                    item.Amount += amount;
                    return;
                }
            }
            Inventory.Add(new InventoryItem() { Type = type, Amount = amount });

            // UpdateProperties();
        }

        public void RemoveItem(ItemType type, int amount) {
            for (var i = 0; i < Inventory.Count; i++) {
                if (Inventory[i].Type == type) {
                    if (Inventory[i].Amount - amount <= 0 || amount == 0) {
                        Inventory.RemoveAt(i);
                    } else {
                        Inventory[i].Amount -= amount;
                    }
                    return;
                }
            }

            // UpdateProperties();
        }

        public bool HasItem(ItemType type) {
            for (var i = 0; i < Inventory.Count; i++) {
                if (Inventory[i].Type == type) {
                    return true;
                }
            }
            return false;
        }

        public int GetItemAmount(ItemType type) {
            for (var i = 0; i < Inventory.Count; i++) {
                if (Inventory[i].Type == type) {
                    return Inventory[i].Amount;
                }
            }
            return 0;
        }

        public int GetTotalItemsAmount() {
            var totalAmount = 0;
            foreach (var collectedObject in CollectedObjects) {
                totalAmount += collectedObject.Value;
            }
            return totalAmount;
        }

#endregion

#region Serialization

        public void Pack(ByteBuffer buffer) {
            SerializationUtils.Put(buffer, VERSION);
            SerializationUtils.Put(buffer, PlayerName);
            SerializationUtils.Put(buffer, WorldSeed);
            SerializationUtils.Put(buffer, TotalGameTime);
            SerializationUtils.Put(buffer, DistanceTraveled);
            SerializationUtils.Put(buffer, Cash);
            SerializationUtils.Put(buffer, Vector3Serializer<Vector3>.Instance, PlayerPosition);
            SerializationUtils.Put(buffer, Perks.Count);
            foreach (var perk in Perks) {
                perk.Pack(buffer);
            }
            SerializationUtils.Put(buffer, Inventory.Count);
            foreach (var item in Inventory) {
                item.Pack(buffer);
            }
            SerializationUtils.Put(buffer, CollectedObjects.Count);
            foreach (var keyValue in CollectedObjects) {
                SerializationUtils.Put(buffer, (int) keyValue.Key);
                SerializationUtils.Put(buffer, keyValue.Value);
            }
            SerializationUtils.Put(buffer, TutorialState);
            SerializationUtils.Put(buffer, SoundVolume);
            SerializationUtils.Put(buffer, MusicVolume);
        }

        public void Unpack(ByteBuffer buffer) {
            var version = SerializationUtils.Get(buffer, 0);
            PlayerName = SerializationUtils.Get(buffer, "");
            WorldSeed = SerializationUtils.Get(buffer, -1);
            TotalGameTime = SerializationUtils.Get(buffer, 0f);
            DistanceTraveled = SerializationUtils.Get(buffer, 0f);
            Cash = SerializationUtils.Get(buffer, 0);
            PlayerPosition = SerializationUtils.Get(buffer, Vector3Serializer<Vector3>.Instance);
            Perks.Clear();
            var l = SerializationUtils.Get(buffer, 0);
            for (var i = 0; i < l; i++) {
                var perk = new Perk();
                perk.Unpack(buffer);
                Perks.Add(perk);
            }
            l = SerializationUtils.Get(buffer, 0);
            for (var i = 0; i < l; i++) {
                var item = new InventoryItem();
                item.Unpack(buffer);
                Inventory.Add(item);
            }
            l = SerializationUtils.Get(buffer, 0);
            for (var i = 0; i < l; i++) {
                var key = Cash = SerializationUtils.Get(buffer, 0);
                var value = SerializationUtils.Get(buffer, 0);
                CollectedObjects.Add((ItemType) key, value);
            }
            TutorialState = SerializationUtils.Get(buffer, 0);
            SoundVolume = SerializationUtils.Get(buffer, 1f);
            MusicVolume = SerializationUtils.Get(buffer, 1f);
        }

        public int GetStructureLength() {
            var perksSize = SerializationUtils.SIZE_OF_INT;
            foreach (var perk in Perks) {
                perksSize += perk.GetStructureLength();
            }

            var inventorySize = SerializationUtils.SIZE_OF_INT;
            foreach (var item in Inventory) {
                inventorySize += item.GetStructureLength();
            }

            var collectedSize = SerializationUtils.SIZE_OF_INT;
            foreach (var item in CollectedObjects) {
                collectedSize += SerializationUtils.SIZE_OF_INT * 2;
            }

            return
                SerializationUtils.SIZE_OF_INT +
                SerializationUtils.GetStringLength(PlayerName, true) + 
                SerializationUtils.SIZE_OF_INT +
                SerializationUtils.SIZE_OF_FLOAT +
                SerializationUtils.SIZE_OF_FLOAT +
                SerializationUtils.SIZE_OF_INT +
                Vector3Serializer<Vector3>.SIZE_OF_STRUCTURE +
                perksSize +
                inventorySize +
                collectedSize +
                SerializationUtils.SIZE_OF_INT +
                SerializationUtils.SIZE_OF_FLOAT +
                SerializationUtils.SIZE_OF_FLOAT;
        }

#endregion

    }
}