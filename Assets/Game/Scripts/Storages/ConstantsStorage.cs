using UnityEngine;

namespace Scripts.Storages {
    [CreateAssetMenu(fileName = "Constants", menuName = "Storages/Constants", order = 1)]
    public class ConstantsStorage : ScriptableObject {
        [Header("Core Settings")]
        public float SaveTimePeriod = 5f;
        public float WorldTransposeStep = 100f;
        public float GatherableGenerationStep = 0.1f;
        public float[] RandomizationWeights = new float[] {
            1.5124f,
            0.3459f,
            0.8332f,
            2.5651f,
            3.9124f,
            15.123f,
            11.333f,
            2.3316f,
            4.6623f,
            23.456f,
            14.272f,
            87.737f,
            0.7f,
            0.7f
        };
        [Space(20)]

        [Header("Player Stats")]
        public float PlayerSpeed = 3f;
        public int PlayerBasketCapacity = 12;
        public float PlayerGatheringTime = 0.5f;
        public float PlayerGatheringRadius = 0.5f;
        [Space(20)]

        [Header("Perk Modifiers")]
        public float BootsSpeedMultiplier = 1.5f;
        public int BigBasketCapacityMultiplier = 3;
        public float StickGatheringRadius = 1.5f;
        public float DrumSpeedMultiplier = 1.5f;
        public float GlovesGatheringTime = 0.2f;
        [Space(20)]

        [Header("Animation")]
        public float RotationSmoothApplyTime = 0.3f;
        public float VelocitySmoothApplyTime = 0.15f;
        [Space(20)]

        [Header("Location Borders")]
        public float BackEdgeOffset = -18f;
        [Space(20)]

        [Header("Camera")]
        public float CameraPositionSmoothApplyTime = 0.3f;
        public float CameraVelocityShiftMultiplier = 0.2f;
        [Space(20)]

        [Header("Rabbits")]
        public float RabbitsCap = 15;
        public float RabbitBehaviorChangePeriod = 3f;
        public Vector2 RabbitAreaBounds = new Vector2(30f, 0f);
        [Space(20)]

        [Header("Vehicle")]
        public float VehicleTargetPositionZOffset = -14f;
        public float VehicleQueueOffset = -4.4f;
        public float VehicleSpeed = 2f;
        [Space(20)]

        [Header("Shop")]
        public Vector3 ShopPositionOffset = new Vector3(1.5f, 0f, 5f);
        public float ShopFirstAppearingDistance = 35f;
        public float ShopAppearingDistancePeriodic = 100f;
        [Space(20)]

        [Header("Buddy")]
        public float BuddyBehaviorChangePeriod = 3f;
        public float BuddyCooldownAfterStrike = 30f;
        public float BuddyCooldownAfterEndOfDialog = 60f;
        public float BuddyStrikeDistance = 0.8f;
        public Vector3 BuddyBubbleWorldOffsetFromCharacterPivot = new Vector3(0f, 0.8f, 0f);
        public int MessagesCountToBuddyRetreat = 5;
        [Space(20)]

        [Header("Boar")]
        public float BoarCooldown = 30f;
        public int BoarApproachesPerRaid = 5;
        public float BoarBehaviorChangePeriod = 5f;
        public float BoarAffectRabbitCheckPeriod = 0.5f;
        public float BoarAffectRabbitDistance = 2f;
        public float BoarRabbitRetreatTime = 10f;
        public float BoarDirectionSpreadAngle = 45f;
        public float BoarDirectionSpreadAngleStep = 45f;
        public float BoarFirstAppearedDistance = 200f;
    }
}
