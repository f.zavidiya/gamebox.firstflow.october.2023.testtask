using System.Collections.Generic;
using Scripts.Enums;
using UnityEngine;

namespace Scripts.Storages {
    [CreateAssetMenu(fileName = "Items", menuName = "Storages/Items", order = 1)]
    public class ItemsStorage : ScriptableObject {
        [System.Serializable]
        public class Item {
            public ItemType TypeOfItem;
            public Sprite IconSprite;
            public string Name;
            public string Description;
            public bool Unique;
            public string AssetPath;
            public int SellPrice;
            public List<float> BiomeSpawnChance;
        }

        [System.Serializable]
        public class Offer {
            public ItemType TypeOfItem;
            public Sprite IconSprite;
            public string Name;
            public int Amount;
            public int Cost;
        }

        [Header("Gatherable Items")]
        public List<Item> Items = new List<Item>();
        [Space(20)]

        [Header("Perks")]
        public List<Offer> ShopOffers = new List<Offer>();

        public Item GetItemByType(ItemType type) {
            foreach (var item in Items) {
                if (item.TypeOfItem == type) {
                    return item;
                }
            }
            return null;
        }
    }
}
