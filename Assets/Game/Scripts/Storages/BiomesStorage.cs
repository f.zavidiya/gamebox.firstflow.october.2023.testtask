using System.Collections.Generic;
using Scripts.Enums;
using UnityEngine;

namespace Scripts.Storages {
    [CreateAssetMenu(fileName = "Biomes", menuName = "Storages/Biomes", order = 1)]
    public class BiomesStorage : ScriptableObject {
        [System.Serializable]
        public class Biome {
            public BiomeType TypeOfBiome;
            public float EndDistance;
            public float TreeDensityPerChunk; // TODO: must be used in generation
            public float BushDensityPerChunk; // TODO: must be used in generation
            public float GatherablesDensityPerStep;
            public string GroundAssetPath;
            public string ChunkAssetPath;
            public Material GroundMaterial;
        }

        [Header("Biomes")]
        public List<Biome> Biomes = new List<Biome>();

        public Biome GetBiomeByType(BiomeType type) {
            foreach (var biome in Biomes) {
                if (biome.TypeOfBiome == type) {
                    return biome;
                }
            }
            return null;
        }
    }
}
