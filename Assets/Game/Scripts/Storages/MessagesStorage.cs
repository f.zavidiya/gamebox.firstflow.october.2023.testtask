using Scripts.Enums;
using UnityEngine;

namespace Scripts.Storages {
    [CreateAssetMenu(fileName = "Messages", menuName = "Storages/Messages", order = 1)]
    public class MessagesStorage : ScriptableObject {
        [System.Serializable]
        public class Message {
            public int Id;
            public MessageType Type;
            public string Text;
        }

        [Header("Buddy")]
        public Message[] BuddyMessages = new Message[] {};
        
        private int _randomBuddyMessagesCount;

        public Message GetRandomBuddyMessage() {
            if (_randomBuddyMessagesCount == 0) {
                foreach (var message in BuddyMessages) {
                    if (message.Type == MessageType.Random) {
                        _randomBuddyMessagesCount++;
                    }
                }
            }

            var randomIndex = Random.Range(0, _randomBuddyMessagesCount);
            var index = 0;
            foreach (var message in BuddyMessages) {
                if (message.Type == MessageType.Random) {
                    if (index >= randomIndex) {
                        return message;
                    }
                    index++;
                }
            }

            return null;
        }

        public Message GetBuddyMessageById(int id, int order = 0) {
            var counter = 0;
            foreach (var message in BuddyMessages) {
                if (message.Id == id) {
                    if (order == counter) {
                        return message;
                    }
                    counter++;
                }
            }

            return null;
        }
    }
}
