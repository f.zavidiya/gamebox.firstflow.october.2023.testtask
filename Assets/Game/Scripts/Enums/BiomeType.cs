namespace Scripts.Enums {
    public enum BiomeType {
        Forest = 0,
        Swamp = 1,
        Tundra = 2,
        Arctic = 3
    }
}