namespace Scripts.Enums {
    public enum ItemType {
        None = 0,
        Mushroom1 = 1,
        Mushroom2 = 2,
        Mushroom3 = 3,
        Mushroom4 = 4,
        Berry1 = 5,
        Berry2 = 6,
        Berry3 = 7,
        Berry4 = 8,
        Carrot = 9,
        Stick = 10,
        Boots = 11,
        Whistle = 12,
        Drum = 13,
        Car = 14,
        BigBasket = 15,
        Gloves = 16,
        Mushroom5 = 17,
        Mushroom6 = 18,
        Mushroom7 = 19
    }
}