namespace Scripts.Enums {
    public enum AnimalBehaviorState {
        None = 0,
        Idle = 1,
        MovingToTarget = 2,
        ReturnToIdle = 3,
        Retreat = 4,
        KnockDown = 5
    }
}