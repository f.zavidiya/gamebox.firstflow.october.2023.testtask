namespace Scripts.Enums {
    public enum PerkType {
        None = 0,
        Perk1 = 1,
        Perk2 = 2,
        Perk3 = 3
    }
}