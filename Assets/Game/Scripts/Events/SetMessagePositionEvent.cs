using CodePatterns.Events;
using UnityEngine;

namespace Scripts.Events {
    public class SetMessagePositionEvent : IEvent {
        public Vector3 Position;
    }
}