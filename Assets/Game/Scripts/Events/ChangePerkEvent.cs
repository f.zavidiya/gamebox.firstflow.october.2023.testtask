using CodePatterns.Events;
using Scripts.Enums;
using UnityEngine;

namespace Scripts.Events {
    public class ChangePerkEvent : IEvent {
        public ItemType ItemType;
        public Vector3 Position;
    }
}