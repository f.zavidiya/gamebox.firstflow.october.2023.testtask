using CodePatterns.Events;

namespace Scripts.Events {
    public class SetSoundVolumeEvent : IEvent {
        public bool IsMusic;
        public float Volume;
    }
}