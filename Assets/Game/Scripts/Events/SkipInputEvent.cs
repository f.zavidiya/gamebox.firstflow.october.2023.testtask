using CodePatterns.Events;

namespace Scripts.Events {
    public class SkipInputEvent : IEvent {
        public bool StateChanged;
        public bool Hide;
    }
}