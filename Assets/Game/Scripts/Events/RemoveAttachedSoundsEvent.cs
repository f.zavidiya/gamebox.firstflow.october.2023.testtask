using CodePatterns.Events;
using UnityEngine;

namespace Scripts.Events {
    public class RemoveAttachedSoundsEvent : IEvent {
        public Transform AttachedTo;
    }
}