using CodePatterns.Events;
using UnityEngine;

namespace Scripts.Events {
    public class PlayerMoveInputEvent : IEvent {
        public Vector2 Direction;
    }
}