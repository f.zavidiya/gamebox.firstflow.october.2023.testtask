using CodePatterns.Events;

namespace Scripts.Events {
    public class ShowMessageEvent : IEvent {
        public string MessageText;
    }
}