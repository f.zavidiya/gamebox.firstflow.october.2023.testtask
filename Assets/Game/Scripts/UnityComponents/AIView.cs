using Scripts.Enums;
using UnityEngine;

namespace Scripts.UnityComponents {
    public class AIView : CharacterView {
        public AnimalBehaviorState BehaviorState = AnimalBehaviorState.Idle;
        public Transform Target;
        public float ConsumingDistance;

        public int Index;
        public float LastBehaviorStateChangeTime;
        public ItemType CarryingItemType = ItemType.None;

        public override bool IsFull { get { return CarryingItemType != ItemType.None; } }

        public override void Clear() {
            base.Clear();
            BehaviorState = AnimalBehaviorState.Idle;
            Target = null;
            LastBehaviorStateChangeTime = Time.time;
            CarryingItemType = ItemType.None;
        }
    }
}