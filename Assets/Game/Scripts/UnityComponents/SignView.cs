using CodePatterns.ObjectPool;
using TMPro;
using UnityEngine;

namespace Scripts.UnityComponents {
    public class SignView : PoolableObjectBase {
        public TMP_Text Label;

        public void SetValue(float value) {
            Label.text = ((int) Mathf.Floor(value / 100f)).ToString();
        }
    }
}