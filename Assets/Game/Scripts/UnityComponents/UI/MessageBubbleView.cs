using CodePatterns.ObjectPool;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Scripts.UnityComponents {
    public class MessageBubbleView : PoolableObjectBase {
        [SerializeField]
        private RectTransform _self;
        [SerializeField]
        private RectTransform[] _arrows;
        [SerializeField]
        private TMP_Text _label;
        
        private CanvasScaler _canvasScaler;
        private Camera _mainCamera;

        public void Awake() {
            _canvasScaler = FindObjectOfType<CanvasScaler>();
            _mainCamera = Camera.main;
        }

        public void Update() {
            _self.sizeDelta = new Vector2(_label.rectTransform.rect.width, _label.rectTransform.rect.height);
        }

        public void SetMessage(string value) {
            _label.text = value;
        }

        public void SetWorldAnchor(Vector3 position) {
            var screenPosition = _mainCamera.WorldToScreenPoint(position);
            SetAnchor(new Vector2(screenPosition.x, screenPosition.y));
        }
        
        public void SetAnchor(Vector2 position) {
            var referenceResolution = _canvasScaler.referenceResolution;
            var localPosition = _self.parent.InverseTransformPoint(position);
            var anchoredPosition = new Vector2(localPosition.x, localPosition.y);
            var dockSignX = position.x > referenceResolution.x / 2f ? 0 : 1;
            var dockSignY = position.y > referenceResolution.y / 2f ? 0 : 2;
            anchoredPosition += new Vector2(dockSignX * (_self.rect.width + 10f), dockSignY * (_self.rect.height / 2f + 60f));
            _self.anchoredPosition = anchoredPosition;

            for (var i = 0; i < _arrows.Length; i++) {
                var arrow = _arrows[i];
                var isActive = i == dockSignX + dockSignY;
                if (arrow.gameObject.activeSelf != isActive) {
                    arrow.gameObject.SetActive(isActive);
                }
            }
        }
    }
}