using System.Collections.Generic;
using Scripts.Enums;
using Scripts.Storages;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Scripts.UnityComponents {
    public class CodexWindowView : MonoBehaviour, IPointerDownHandler {
        public List<CodexPageView> Pages;
        public FixedSliderView Slider;
        public RectTransform ContentHolder;
        public GameObject PagePrefab;

        public void Init(List<ItemsStorage.Item> items) {
            foreach (var item in items) {
                var page = Instantiate(PagePrefab).GetComponent<CodexPageView>();
                page.transform.SetParent(ContentHolder);
                page.transform.localScale = Vector3.one;
                page.ItemType = item.TypeOfItem;
                page.IconImage.sprite = item.IconSprite;
                page.NameLabel.text = item.Name;
                page.DescriptionLabel.text = item.Description;
                Pages.Add(page);
            }
        }

        public void Open(ItemType itemType) {
            var index = 0;
            
            for (var i = 0; i < Pages.Count; i++) {
                if (Pages[i].ItemType == itemType) {
                    index = i;
                    i = Pages.Count;
                }
            }

            Slider.CurrentSlide = index;

            gameObject.SetActive(true);
        }

        public void Close() {
            gameObject.SetActive(false);
        }

        public void OnPointerDown(PointerEventData eventData) {
            Close();
        }
    }
}