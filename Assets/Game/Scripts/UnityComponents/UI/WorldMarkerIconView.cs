using CodePatterns.ObjectPool;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Scripts.UnityComponents {
    public class WorldMarkerIconView : PoolableObjectBase {
        public RectTransform Transform;
        public Image IconImage;
        public Image ValueImage;
        public TMP_Text TimeLabel;
        public bool HideInScreenBounds;

        public int TargetHash;

        private CanvasScaler _canvasScaler;
        public Camera _mainCamera;

        private float _lastTime;

        public void UpdateValue(float value) {
            ValueImage.fillAmount = value;
        }

        public void UpdateTime(float time) {
            if (Mathf.Abs(_lastTime - time) < 1f) {
                return;
            }

            _lastTime = time;
            if (time > 0f) {
                if (!TimeLabel.gameObject.activeSelf) {
                    TimeLabel.gameObject.SetActive(true);
                }
                var s = time % 60f;
                var m = (time - s) / 60f;
                TimeLabel.text = $"{(int)m:D2}:{(int)s:D2}";
            } else {
                if (TimeLabel.gameObject.activeSelf) {
                    TimeLabel.gameObject.SetActive(false);
                }
            }
        }

        public void UpdatePosition(Vector3 worldPosition) {
            if (_canvasScaler == null) {
                _canvasScaler = FindObjectOfType<CanvasScaler>();
            }

            if (_mainCamera == null) {
                _mainCamera = Camera.main;
            }

            var viewportPosition = _mainCamera.WorldToViewportPoint(worldPosition);
            var backward = viewportPosition.z < 0f;
            if (backward || viewportPosition.x < 0f || viewportPosition.x > 1f || viewportPosition.y < 0f || viewportPosition.y > 1f) {
                viewportPosition.x -= 0.5f;
                viewportPosition.y -= 0.5f;
                var viewportMaxPointerOffsetUnsigned = Mathf.Max(Mathf.Abs(viewportPosition.x), Mathf.Abs(viewportPosition.y));
                var sign = backward ? -1f : 1f;
                viewportPosition.x = viewportPosition.x / viewportMaxPointerOffsetUnsigned * 0.5f * sign + 0.5f;
                viewportPosition.y = viewportPosition.y / viewportMaxPointerOffsetUnsigned * 0.5f * sign + 0.5f;
            }
            var anchoredPosition = viewportPosition * FindObjectOfType<CanvasScaler>().referenceResolution;
            var clampedAnchoredPosition = anchoredPosition;
            clampedAnchoredPosition.x = Mathf.Clamp(anchoredPosition.x, 40f, _canvasScaler.referenceResolution.x - 40f);
            clampedAnchoredPosition.y = Mathf.Clamp(anchoredPosition.y, 40f, _canvasScaler.referenceResolution.y - 40f);
            Transform.anchoredPosition = clampedAnchoredPosition;
            var outOfScreen = (anchoredPosition - clampedAnchoredPosition).sqrMagnitude > 10f;
            IconImage.enabled = !HideInScreenBounds || outOfScreen;
        }

        public override void Clear() {
            base.Clear();
        }
    }
}