using UnityEngine;
using UnityEngine.EventSystems;

namespace Scripts.UnityComponents {
    public class FixedSliderView : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerMoveHandler {
        public RectTransform Transform;
        public RectTransform ContentTransform;
        public int CurrentSlide = 0;

        private Vector2 _startPosition;
        private Vector2 _startPointerPosition;
        private bool _pressed;

        public void OnPointerDown(PointerEventData eventData) {
            _startPosition = ContentTransform.anchoredPosition;
            _startPointerPosition = eventData.position;
            _pressed = true;
        }

        public void OnPointerMove(PointerEventData eventData) {
            if (_pressed) {
                var x = _startPosition.x + eventData.position.x - _startPointerPosition.x;
                if (x > 0f) {
                    x = 0f;
                }
                if (x < -ContentTransform.rect.width + Transform.rect.width) {
                    x = -ContentTransform.rect.width + Transform.rect.width;
                }
                ContentTransform.anchoredPosition = new Vector2(x, ContentTransform.anchoredPosition.y);
            }
        }

        public void OnPointerUp(PointerEventData eventData) {
            _pressed = false;
            CurrentSlide = (int) Mathf.Round(-ContentTransform.anchoredPosition.x / Transform.rect.width);
        }

        public void Update() {
            if (!_pressed) {
                var targetPosition = new Vector2(-CurrentSlide * Transform.rect.width, ContentTransform.anchoredPosition.y);
                ContentTransform.anchoredPosition = Vector2.Lerp(ContentTransform.anchoredPosition, targetPosition, Time.fixedDeltaTime * 10f);
            }
        }
    }
}