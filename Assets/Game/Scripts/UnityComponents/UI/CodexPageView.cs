using Scripts.Enums;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Scripts.UnityComponents {
    public class CodexPageView : MonoBehaviour {
        public ItemType ItemType;
        public Image IconImage;
        public TMP_Text NameLabel;
        public TMP_Text DescriptionLabel;
    }
}