using Scripts.Structures;
using TMPro;
using UnityEngine;

namespace Scripts.UnityComponents {
    public class StatsView : MonoBehaviour {
        public TMP_Text TraveledDistanceValueLabel;
        public TMP_Text TimeValueLabel;
        public TMP_Text CashValueLabel;

        public void SetStats(GameStoredState gameStoredState, GameRuntimeState gameRuntimeState) {
            TraveledDistanceValueLabel.text = $"{(gameStoredState.DistanceTraveled / 100f):N1} км.";
            var time = gameStoredState.TotalGameTime + Time.time - gameRuntimeState.LastSaveTime;
            var seconds = (int) (time % 60f);
            var minutes = (int) ((time - time % 60f) / 60f % 60);
            var hours = (int) ((time - time % 3600f) / 3600f);
            TimeValueLabel.text = $"{hours:D2}:{minutes:D2}:{seconds:D2}";
            CashValueLabel.text = $"{gameStoredState.Cash} руб.";
        }
    }
}
