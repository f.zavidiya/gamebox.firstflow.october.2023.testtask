using System.Collections.Generic;
using CodePatterns.Events;
using Scripts.Enums;
using Scripts.Events;
using Scripts.Storages;
using Scripts.Structures;
using UnityEngine;
using UnityEngine.UI;

namespace Scripts.UnityComponents {
    public class HUDView : MonoBehaviour {
        public EventBus EventBus;
        public Camera MainCamera;
        public ItemCounterView CashCounter;
        public List<ItemCounterView> GatherableCounters = new List<ItemCounterView>();
        public List<ItemCounterView> PerkCounters = new List<ItemCounterView>();

        public ItemCounterView BasketCounter;

        public RectTransform GatherableCountersHolder;
        public GameObject GatherableCounterPrefab;

        public RectTransform PerkCountersHolder;
        public GameObject PerkCounterPrefab;
        
        public RectTransform OffersPanel;
        public List<ItemOfferView> Offers = new List<ItemOfferView>();
        public RectTransform ConsumableOffersHolder;
        public RectTransform UniqueOffersHolder;
        public GameObject OfferPrefab;

        public CodexWindowView CodexWindow;

        public MessageBubbleView MessageBubble;

        public RectTransform MainMenu;
        public StatsView StatsPanel;
        public Button ExitButton;
        public Button ContinueButton;
        public Button NewGameButton;
        public Button CloseMainMenuButton;
        public Slider SoundVolumeSlider;
        public Slider MusicVolumeSlider;

        public LinkedList<ItemOfferView> CalledOffers = new LinkedList<ItemOfferView>();

        public void Awake() {
            ExitButton.onClick.AddListener(ApplicationExit);
            CloseMainMenuButton.onClick.AddListener(SwitchMainMenu);
            ContinueButton.onClick.AddListener(SwitchMainMenu);
            NewGameButton.onClick.AddListener(StartNewGame);
            SoundVolumeSlider.onValueChanged.AddListener(OnChangeSoundSlider);
            MusicVolumeSlider.onValueChanged.AddListener(OnChangeMusicSlider);
            MainMenu.gameObject.SetActive(false);

            foreach (var counter in GatherableCounters) {
                var screenPosition = MainCamera.ViewportToScreenPoint(new Vector3(0.5f, 0.5f, 0f));
                counter.SetCount(0, screenPosition);
            }

            CodexWindow.Close();
        }

        public void OnDestroy() {
            ExitButton.onClick.RemoveListener(ApplicationExit);
            CloseMainMenuButton.onClick.RemoveListener(SwitchMainMenu);
            ContinueButton.onClick.RemoveListener(SwitchMainMenu);
            NewGameButton.onClick.RemoveListener(StartNewGame);
            SoundVolumeSlider.onValueChanged.RemoveListener(OnChangeSoundSlider);
            MusicVolumeSlider.onValueChanged.RemoveListener(OnChangeMusicSlider);

            foreach (var offer in Offers) {
                offer.OnOfferClick -= OnClickOffer;
                offer.OnHelpClick -= OnClickHelp;
            }
        }

        public void SetVolume(float soundVolume, float musicVolume) {
            SoundVolumeSlider.value = soundVolume;
            MusicVolumeSlider.value = musicVolume;
        }

        public void SetCash(int value) {
            var screenPosition = MainCamera.ViewportToScreenPoint(new Vector3(0.5f, 0.5f, 0f));
            CashCounter.SetCount(value, screenPosition);
        }

        public void SetGatherableCounter(ItemType itemType, int value, Vector3 worldPosition) {
            foreach (var counter in GatherableCounters) {
                if (counter.ItemType == itemType) {
                    var screenPosition = worldPosition;
                    if (!worldPosition.Equals(Vector3.zero)) {
                        screenPosition = MainCamera.WorldToScreenPoint(worldPosition);
                    }
                    counter.SetCount(value, screenPosition);
                }
            }
        }

        public void ShowBasketBlocked() {
            BasketCounter.ShowBlockedStateEffect();
        }

        public void SetPerkCounter(ItemType itemType, int value, Vector3 worldPosition) {
            foreach (var counter in PerkCounters) {
                if (counter.ItemType == itemType) {
                    var screenPosition = worldPosition;
                    if (!worldPosition.Equals(Vector3.zero)) {
                        screenPosition = MainCamera.WorldToScreenPoint(worldPosition);
                    }
                    counter.SetCount(value, screenPosition);
                }
            }
        }

        public void ShowGoods() {
            if (!OffersPanel.gameObject.activeSelf) {
                OffersPanel.gameObject.SetActive(true);
            }
        }

        public void HideGoods() {
            if (OffersPanel.gameObject.activeSelf) {
                OffersPanel.gameObject.SetActive(false);
            }
        }

        public void UpdateOffers(List<InventoryItem> items) {
            foreach (var offer in Offers) {
                var amount = 0;
                foreach (var item in items) {
                    if (offer.ItemType == item.Type) {
                        amount += item.Amount;
                    }
                }
                offer.SetPurchasedState(amount);
            }
        }

        public void UpdateBasketCounter(int value) {
            BasketCounter.SetCount(value, Vector3.zero);
        }

        public void UpdateBasketCapacity(int value) {
            BasketCounter.SetCapacity(value);
        }

        public void InitItems(List<ItemsStorage.Item> items) {
            CodexWindow.Init(items);

            foreach (var item in items) {
                if (item.SellPrice > 0) {
                    var counter = Instantiate(GatherableCounterPrefab).GetComponent<ItemCounterView>();
                    counter.HUDView = this;
                    counter.ItemType = item.TypeOfItem;
                    counter.SetIcon(item.IconSprite);
                    counter.transform.SetParent(GatherableCountersHolder);
                    counter.transform.localScale = Vector3.one;
                    GatherableCounters.Add(counter);
                } else {
                    var counter = Instantiate(PerkCounterPrefab).GetComponent<ItemCounterView>();
                    counter.HUDView = this;
                    counter.ItemType = item.TypeOfItem;
                    counter.SetIcon(item.IconSprite);
                    counter.transform.SetParent(PerkCountersHolder);
                    counter.transform.localScale = Vector3.one;
                    PerkCounters.Add(counter);
                }
            }
        }

        public void InitOffers(List<ItemsStorage.Offer> offers, List<ItemsStorage.Item> items) {
            foreach (var offer in Offers) {
                offer.OnOfferClick -= OnClickOffer;
                offer.OnHelpClick -= OnClickHelp;
                Destroy(offer.gameObject);
            }
            Offers.Clear();

            foreach (var offer in offers) {
                foreach (var item in items) {
                    if (offer.TypeOfItem == item.TypeOfItem) {
                        var offerView = Instantiate(OfferPrefab).GetComponent<ItemOfferView>();
                        offerView.ItemType = offer.TypeOfItem;
                        offerView.TitleLabel.text = offer.Name;
                        offerView.IconImage.sprite = offer.IconSprite;
                        offerView.Cost = offer.Cost;
                        offerView.Amount = offer.Amount;
                        offerView.Unique = item.Unique;
                        if (offerView.Unique) {
                            offerView.transform.SetParent(UniqueOffersHolder);
                        } else {
                            offerView.transform.SetParent(ConsumableOffersHolder);
                        }
                        offerView.transform.localScale = Vector3.one;
                        Offers.Add(offerView);
                    }
                }
            }

            foreach (var offer in Offers) {
                offer.OnOfferClick += OnClickOffer;
                offer.OnHelpClick += OnClickHelp;
            }
        }

        public void SwitchMainMenu() {
            MainMenu.gameObject.SetActive(!MainMenu.gameObject.activeSelf);
        }

        public void StartNewGame() {
            EventBus.RaiseEvent(EventBus.Get<NewGameEvent>());
        }

        public void SetStats(GameStoredState gameStoredState, GameRuntimeState gameRuntimeState) {
            if (MainMenu.gameObject.activeSelf) {
                StatsPanel.SetStats(gameStoredState, gameRuntimeState);
            }
        }

        private void OnClickOffer(ItemOfferView offer) {
            CalledOffers.AddLast(offer);
        }

        private void OnClickHelp(ItemOfferView offer) {
            CodexWindow.Open(offer.ItemType);
        }
        
        private void OnChangeSoundSlider(float value) {
            var soundVolumeEvent = EventBus.Get<SetSoundVolumeEvent>();
            soundVolumeEvent.IsMusic = false;
            soundVolumeEvent.Volume = SoundVolumeSlider.value;
            EventBus.RaiseEvent(soundVolumeEvent);
        }

        private void OnChangeMusicSlider(float value) {
            var soundVolumeEvent = EventBus.Get<SetSoundVolumeEvent>();
            soundVolumeEvent.IsMusic = true;
            soundVolumeEvent.Volume = MusicVolumeSlider.value;
            EventBus.RaiseEvent(soundVolumeEvent);
        }

        private void ApplicationExit() {
            EventBus.RaiseEvent(EventBus.Get<SaveEvent>());
            Application.Quit(0);
        }
    }
}