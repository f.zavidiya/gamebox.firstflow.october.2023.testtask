using DG.Tweening;
using System.Collections;
using Scripts.Enums;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Scripts.UnityComponents {
    public class ItemCounterView : MonoBehaviour, IPointerDownHandler {
        public ItemType ItemType;
        public bool AlwaysVisible;

        public HUDView HUDView;

        [SerializeField]
        private Image _counterIcon;
        [SerializeField]
        private Image _counterIconForFly;
        [SerializeField]
        private TMP_Text _counterLabel;
        [SerializeField]
        private TMP_Text _capacityLabel;
        [SerializeField]
        private Image _fullStateIcon;

        private int _count;
        private int _capacity;
        private Coroutine _delayedSetActiveCoroutine;
        private bool _isMaterialInstantiated;

        public void SetIcon(Sprite sprite) {
            _counterIcon.sprite = sprite;
            _counterIconForFly.sprite = _counterIcon.sprite;
        }

        public void SetCount(int value, Vector3 screenPosition) {
            var delta = value - _count;
            _count = value;

            var isActive = _count > 0 || AlwaysVisible;

            if (gameObject.activeSelf != isActive) {
                if (_delayedSetActiveCoroutine != null) {
                    StopCoroutine(_delayedSetActiveCoroutine);
                    _delayedSetActiveCoroutine = null;
                }
                if (!isActive) {
                    _delayedSetActiveCoroutine = StartCoroutine(DelayedSetActive(false, 0.2f));
                } else {
                    gameObject.SetActive(true);
                }
            }

            if (isActive) {
                _counterLabel.text = _count.ToString();
            }

            UpdateFullState();

            if (screenPosition.Equals(Vector3.zero)) {
                screenPosition = _counterIcon.transform.TransformPoint(Vector3.zero) + new Vector3(0f, 50f, 0f);
            }

            if (delta > 0) {
                IconFly(screenPosition, true);
            } else {
                IconFly(screenPosition, false);
            }
        }

        public void ShowBlockedStateEffect() {
            if (!_isMaterialInstantiated) {
                _isMaterialInstantiated = true;
                _counterIcon.material = new Material(_counterIcon.material);
            }

            _counterIcon.transform.DOScale(2f, 0.2f).onComplete += () => {
                _counterIcon.transform.DOScale(1f, 0.2f);
            };

            _counterIcon.material.DOColor(Color.red, 0.2f).onComplete += () => {
                _counterIcon.material.DOColor(Color.white, 0.2f);
            };
        }

        public void SetCapacity(int value) {
            _capacity = value;

            if (_capacityLabel != null) {
                _capacityLabel.text = $"/{_capacity}";
            }

            UpdateFullState();
        }

        public void OnPointerDown(PointerEventData eventData) {
            if (HUDView != null && ItemType != ItemType.None) {
                HUDView.CodexWindow.Open(ItemType);
            }
        }

        private void IconFly(Vector3 screenPosition, bool isIncoming) {
            var iconRectTransform = (RectTransform) _counterIconForFly.transform;
            var anchoredIconPosition = _counterIcon.transform.localPosition;
            var from = iconRectTransform.parent.InverseTransformPoint(screenPosition);
            var to = anchoredIconPosition;
            if (!isIncoming) {
                (from, to) = (to, from);
            }
            _counterIconForFly.enabled = true;
            iconRectTransform.localPosition = from;
            iconRectTransform.DOLocalMove(to, 0.15f, false).onComplete += () => {
                _counterIconForFly.enabled = false;
                if (isIncoming) {
                    _counterIcon.transform.DOScale(1.5f, 0.1f).onComplete += () => {
                        _counterIcon.transform.DOScale(1f, 0.1f);
                    };
                }
            };
        }

        private void UpdateFullState() {
            if (_capacity > 0 && _fullStateIcon != null) {
                var isFull = _count >= _capacity;
                if (_fullStateIcon.enabled != isFull) {
                    _fullStateIcon.enabled = isFull;
                }
            }

        }

        private IEnumerator DelayedSetActive(bool value, float time) {
            yield return new WaitForSeconds(time);
            gameObject.SetActive(value);
            _delayedSetActiveCoroutine = null;
        }
    }
}