using System;
using Scripts.Enums;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Scripts.UnityComponents {
    public class ItemOfferView : MonoBehaviour, IPointerDownHandler {
        public ItemType ItemType;
        public bool Unique;
        public int Amount;
        public int Cost { get { return _cost; } set { _cost = value; CostLabel.text = _cost.ToString(); } }
        public TMP_Text TitleLabel;
        public Image IconImage;
        public Image PurchasedSticker;
        public RectTransform AmountSticker;
        public TMP_Text AmountStickerLabel;
        public TMP_Text CostLabel;
        public Button HelpButton;

        public Action<ItemOfferView> OnOfferClick;
        public Action<ItemOfferView> OnHelpClick;

        private int _cost;

        public void Awake() {
            HelpButton.onClick.AddListener(HelpClick);
        }

        ~ItemOfferView() {
            HelpButton.onClick.RemoveListener(HelpClick);
        }

        public void SetPurchasedState(int value) {
            PurchasedSticker.enabled = value > 0 && Unique;
            
            var amountStickerActive = value > 0 && !Unique;
            
            if (AmountSticker.gameObject.activeSelf != amountStickerActive) {
                AmountSticker.gameObject.SetActive(amountStickerActive);
            }

            if (amountStickerActive) {
                AmountStickerLabel.text = value.ToString();
            }
        }

        public void OnPointerDown(PointerEventData eventData) {
            OnOfferClick?.Invoke(this);
        }

        public void HelpClick() {
            OnHelpClick?.Invoke(this);
        }
    }
}