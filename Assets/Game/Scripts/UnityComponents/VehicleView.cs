using System.Collections;
using CodePatterns.ObjectPool;
using UnityEngine;

namespace Scripts.UnityComponents {
    public class VehicleView : PoolableObjectBase {
        public Transform Transform;
        public Collider Collider;
        public Transform CargoTransform;
        public float Speed;
        public float RestTime;
        public int Capacity;

        public int Load { get { return _load; } set { _load = value; UpdateLoad(); } }
        public float RestUntilTime { get { return _restStartTime + RestTime; } }
        public bool IsActive { get { return _isActive; } }
        public float LastEngineSoundTime;

        private bool _isActive = false;
        private Vector3 _comingOutStartPosition;
        private float _comingOutTargetZ = -100f;
        private float _restStartTime = -100f;
        private int _load;

        public void SetActive(bool value) {
            if (_isActive != value) {
                _isActive = value;
                if (!_isActive) {
                    StartCoroutine(ComingOut());
                }
            }
        }

        private void UpdateLoad() {
            var value = (float) Load / Capacity;
            var length = CargoTransform.childCount;
            for (var i = 0; i < length; i++) {
                var box = CargoTransform.GetChild(i);
                var visible = (float)i / length <= value;
                if (box.gameObject.activeSelf != visible) {
                    box.gameObject.SetActive(visible);
                }
            }
        }

        private IEnumerator ComingOut() {
            _comingOutStartPosition = Transform.position;
            _comingOutTargetZ = Transform.position.z - 15f;
            _restStartTime = Time.time;
            Collider.enabled = false;
            while (Time.time - _restStartTime < 1f) {
                yield return new WaitForEndOfFrame();
                var value = Time.time - _restStartTime;
                Transform.position = new Vector3(_comingOutStartPosition.x - value * 2f, 0f, _comingOutStartPosition.z + (1f - Mathf.Abs(value - 0.5f) * 2f));
                Transform.rotation = Quaternion.Euler(0f, value * -180f, 0f);
            }
            while (Transform.position.z > _comingOutTargetZ) {
                yield return new WaitForEndOfFrame();
                Transform.position += Vector3.back * Speed * Time.deltaTime;
            }
            Load = 0;
            while (Time.time - RestUntilTime < 0f) {
                yield return new WaitForEndOfFrame();
            }
            _isActive = true;
            Transform.rotation = Quaternion.identity;
            var t = 0f;
            var startX = Transform.position.x;
            while (t < 0.5f) {
                yield return new WaitForEndOfFrame();
                t += Time.deltaTime;
                Transform.position = new Vector3(Mathf.Lerp(startX, 0f, t * 2f), 0f, Transform.position.z);
            }
            Transform.position = new Vector3(0f, 0f, Transform.position.z);
            Collider.enabled = true;
        }
    }
}