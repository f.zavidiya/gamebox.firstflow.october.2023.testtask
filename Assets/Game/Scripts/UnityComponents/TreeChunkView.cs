using UnityEngine;

namespace Scripts.UnityComponents {
    public class TreeChunkView : LandscapeChunkView {
        public Transform[] Trees;
        public float HalfSize = 5f;

        /// <summary>
        /// Set random position of transforms in Trees collection.
        /// </summary>
        /// <param name="seed">Defines deterministic depended pseudo-random results.</param>
        /// <param name="weights">Defines algorythm-specific oscillation amplitudes and frequencies.</param>
        public void Randomize(int seed, float[] weights) {
            for (var i = 0; i < Trees.Length; i++) {
                var tree = Trees[i];
                var position = new Vector3();

                var tan1 = seed * weights[0] + i * weights[1];
                tan1 = Mathf.Tan(tan1 % 2f - 1f);
                tan1 %= 1f;
                var tan2 = seed * weights[2] + i * weights[3];
                tan2 = Mathf.Tan(tan2 % 2f - 1f);
                tan2 %= 1f;

                position.x = (Mathf.Sin(seed * weights[4] + i * weights[5]) * tan1 + Mathf.Cos(i * weights[6] / seed * weights[7])) * (1f - tan2) * HalfSize;
                position.z = (Mathf.Sin(seed * weights[8] + i * weights[9]) * tan2 + Mathf.Cos(i * weights[10] / seed * weights[11]) * (1f - tan1)) * HalfSize;
                tree.transform.localPosition = position;
                tree.transform.localScale = Vector3.one * (weights[12] + tan1 * tan2 * weights[13]);
                tree.transform.localRotation = Quaternion.identity;
                if (Mathf.Abs(tree.transform.position.x) < 2f) {
                    if (tree.gameObject.activeSelf) {
                        tree.gameObject.SetActive(false);
                    }
                } else {
                    if (!tree.gameObject.activeSelf) {
                        tree.gameObject.SetActive(true);
                    }
                }
            }
            
            // Make tilt both of nearby spawned trees
            for (var i = 0; i < Trees.Length; i++) {
                for (var j = 0; j < Trees.Length; j++) {
                    if (i != j) {
                        var leftTree = Trees[i];
                        var rightTree = Trees[j];

                        var delta = leftTree.localPosition - rightTree.localPosition;
                        if (delta.magnitude < 1f) {
                            var moveDelta = delta.normalized * (1f - delta.magnitude) * 0.5f;
                            moveDelta.y = 0f;
                            var moveDeltaMagnitude = moveDelta.magnitude;
                            if (moveDeltaMagnitude > 0.1f) {
                                leftTree.localPosition += moveDelta;
                                rightTree.localPosition -= moveDelta;
                                var tiltRotation = Quaternion.Euler(moveDeltaMagnitude * 45f, 0f, 0f);
                                var yAngle = Quaternion.LookRotation(moveDelta, Vector3.up).eulerAngles.y;
                                leftTree.transform.localRotation = Quaternion.Slerp(leftTree.transform.localRotation, Quaternion.Euler(0f, yAngle + 180f, 0f) * tiltRotation, 0.5f);
                                rightTree.transform.localRotation = Quaternion.Slerp(rightTree.transform.localRotation, Quaternion.Euler(0f, yAngle, 0f) * tiltRotation, 0.5f);
                            }
                        }
                    }
                }
            }
        }
    }
}