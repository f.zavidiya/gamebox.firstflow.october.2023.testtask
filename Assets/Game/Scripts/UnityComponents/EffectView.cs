using System.Collections;
using CodePatterns.ObjectPool;
using UnityEngine;

namespace Scripts.UnityComponents {
    public class EffectView : PoolableObjectBase {
        public ParticleSystem[] Particles;
        public float Duration = 1f;

        public bool Active { get { return _active; } }

        private bool _active;

        public void Play() {
            _active = true;
            StartCoroutine(DeactivateAfterTime());
        }

        public void SetLifetime(float value) {
            foreach (var particle in Particles) {
                particle.Stop();

                var particleSettings = particle.main;
                particleSettings.duration = value + 0.5f;
                particleSettings.startLifetime = value + 0.5f;
            }
        }

        private IEnumerator DeactivateAfterTime() {
            yield return new WaitForEndOfFrame();

            foreach (var particle in Particles) {
                particle.Play();
            }

            yield return new WaitForSeconds(Duration);
            
            _active = false;
        }
    }
}