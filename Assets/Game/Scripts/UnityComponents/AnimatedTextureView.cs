using UnityEngine;

namespace Scripts.UnityComponents {
    public class AnimatedTextureView : MonoBehaviour  {
        public int AnimationSteps;
        public float AnimationStepTime;
        public MeshRenderer Renderer;

        private float _lastStepTime;
        private int _step;
        private int _scalePropertyKey = Shader.PropertyToID("_MainTex_ST");

        public void Update() {
            if (Time.time - _lastStepTime > AnimationStepTime) {
                _lastStepTime = Time.time;

                _step++;
                if (_step >= AnimationSteps) {
                    _step = 0;
                }

                var propertyBlock = new MaterialPropertyBlock();
                Renderer.GetPropertyBlock(propertyBlock);
                propertyBlock.SetVector(_scalePropertyKey, new Vector4(1f, 0.25f, 0f, -0.25f * _step));
                Renderer.SetPropertyBlock(propertyBlock);
            }
        }
    }
}