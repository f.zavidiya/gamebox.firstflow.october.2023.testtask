using System.Collections.Generic;
using CodePatterns.ObjectPool;
using Scripts.Enums;
using UnityEngine;

namespace Scripts.UnityComponents {
    public class CharacterView : PoolableObjectBase {
        public Transform Transform;
        public Rigidbody Rigidbody;
        public Transform BasketMushroomPivot;
        public Transform BasketMushroomFullPivot;
        public Collider TriggerCollider;
        public float Speed;
        public int BasketCapacity;
        public float GatheringTime;

        public virtual bool IsFull { get; private set; }
        public virtual bool IsKnockedDown { get { return _animator.GetBool(_knockDownKey); } }

        public Vector2 Direction;
        public LinkedList<Collider> LatestCollisions = new LinkedList<Collider>();
        public LinkedList<GameObject> MushroomsInBasket = new LinkedList<GameObject>();
        public float LastTransferActionTime;
        public EnvironmentObjectView GatheringTarget;
        public float GatheringStartTime;

        [SerializeField]
        protected Animator _animator;

        protected int _velocityKey = Animator.StringToHash("MovingVelocity");
        protected int _knockDownKey = Animator.StringToHash("KnockDown");
        protected Vector3 _smoothVelocity = Vector3.zero;

        public void SetVelocity(Vector3 velocity) {
            _smoothVelocity = Vector3.Lerp(_smoothVelocity, velocity, Time.fixedDeltaTime * 10f);
            var horizontalVelocity = _smoothVelocity;
            horizontalVelocity.y = 0f;
            _animator.SetFloat(_velocityKey, _smoothVelocity.magnitude);
        }

        public void SetCollectedGatherables(Dictionary<ItemType, int> CollectedObjects) {
            var totalAmount = 0;
            foreach (var keyValue in CollectedObjects) {
                var amount = keyValue.Value;
                totalAmount += amount;
            }
            IsFull = totalAmount >= BasketCapacity;
            BasketMushroomPivot.gameObject.SetActive(totalAmount > 0 && !IsFull);
            BasketMushroomFullPivot.gameObject.SetActive(IsFull);
        }

        public void SetKnockDown(int value = 1) {
            _animator.SetBool(_knockDownKey, value == 1);
        }

        public void OnTriggerEnter(Collider collider) {
            if (!LatestCollisions.Contains(collider)) {
                LatestCollisions.AddLast(collider);
            }
        }

        public void OnTriggerExit(Collider collider) {
            if (LatestCollisions.Contains(collider)) {
                LatestCollisions.Remove(collider);
            }
        }
    }
}