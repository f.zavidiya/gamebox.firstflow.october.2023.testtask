using System.Collections;
using UnityEngine;

namespace Scripts.UnityComponents {
    public class ShopView : EnvironmentObjectView {
        public Animation Animation;
        public AnimationClip OpenAnimationClip;
        public float StartOpeningTime;
        public bool IsOpening;
        public bool IsClosing;
        public bool IsOpened;

        private Coroutine _animationCoroutine;

        public void OpenShop() {
            if (IsOpening) {
                return;
            }

            IsOpening = true;
            if (_animationCoroutine != null && Time.time - StartOpeningTime < 1f) {
                IsClosing = false;
                StopCoroutine(_animationCoroutine);
                StartOpeningTime = Time.time - (1f - (Time.time - StartOpeningTime));
            } else {
                StartOpeningTime = Time.time;
            }

            _animationCoroutine = StartCoroutine(OpenShopAsync());
        }

        public void CloseShop() {
            if (IsClosing) {
                return;
            }

            HideGoods();

            IsClosing = true;
            if (_animationCoroutine != null && Time.time - StartOpeningTime < 1f) {
                IsOpening = false;
                StopCoroutine(_animationCoroutine);
                StartOpeningTime = Time.time - (1f - (Time.time - StartOpeningTime));
            } else {
                StartOpeningTime = Time.time;
            }

            _animationCoroutine = StartCoroutine(CloseShopAsync());
        }

        private IEnumerator OpenShopAsync() {
            while (Time.time - StartOpeningTime < 1f) {
                yield return new WaitForEndOfFrame();

                var t = Time.time - StartOpeningTime;
                OpenAnimationClip.SampleAnimation(gameObject, t);
            }
            
            IsOpening = false;

            ShowGoods();
        }

        private IEnumerator CloseShopAsync() {
            while (Time.time - StartOpeningTime < 1f) {
                yield return new WaitForEndOfFrame();

                var t = Time.time - StartOpeningTime;
                OpenAnimationClip.SampleAnimation(gameObject, 1f - t);
            }
            
            IsClosing = false;
        }

        private void ShowGoods() {
            IsOpened = true;
        }

        private void HideGoods() {
            IsOpened = false;
        }
    }
}