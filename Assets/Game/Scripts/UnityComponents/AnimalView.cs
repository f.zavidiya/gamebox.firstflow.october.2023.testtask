using UnityEngine;

namespace Scripts.UnityComponents {
    public class AnimalView : AIView {
        public Transform TamedEffectTransform;
        public Transform ScaredEffectTransform;
        
        public bool IsTamed { get { return _isTamed; } set { _isTamed = value; UpdateEmotionEffect(); } }
        public bool IsScared { get { return _isScared; } set { _isScared = value; UpdateEmotionEffect(); } }

        private bool _isTamed;
        private bool _isScared;

        public override void Clear() {
            base.Clear();
            IsTamed = false;
        }

        private void UpdateEmotionEffect() {
            ScaredEffectTransform.gameObject.SetActive(_isScared);
            TamedEffectTransform.gameObject.SetActive(_isTamed && !_isScared);
        }
    }
}