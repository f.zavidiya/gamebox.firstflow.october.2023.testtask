using CodePatterns.ObjectPool;
using Scripts.Enums;

namespace Scripts.UnityComponents {
    public class LandscapeChunkView : PoolableObjectBase {
        public BiomeType BiomeType;
    }
}