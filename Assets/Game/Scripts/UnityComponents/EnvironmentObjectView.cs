using CodePatterns.ObjectPool;
using Scripts.Enums;
using UnityEngine;

namespace Scripts.UnityComponents {
    public class EnvironmentObjectView : PoolableObjectBase {
        public ItemType ItemType;
        public Collider TriggerCollider;

        public void Awake() {
            if (TriggerCollider == null) {
                TriggerCollider = GetComponent<Collider>();
            }
        }
    }
}