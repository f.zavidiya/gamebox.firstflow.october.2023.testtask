# GameBox.FirstFlow.October.2023.TestTask



## Введение

**Автор:** Завидия Фёдор Андреевич

**Проект:** Тестовое задание для участия в первом потоке GameBox



## Актуальный билд

01.11.2023: https://disk.yandex.ru/d/dEHjkqANYpnCTQ



## Управление в игре

**W, A, S, D** - Перемещение по локации.

**Клик мышью / Пробел** - Пропуск диалога с NPC.

**Клик мышью в магазине** - Покупка предметов.

**Клик мышью на иконки-счетчики** - Просмотр описания предмета.



## Структура

**/Resources/Prefabs/** - Содержит все префабы, загружаемые в рантайме для создания их экземпляров на сцене.

**/Settings/Input/** - Настройки `InputSystem`.

**/Settings/Storages/** - `ScriptableObject`'ы с конфигурацией предметов, констант, звуков и диалогов.



## Иерархия объектов

**_STARTUP_** - Корневой объект. Содержит компонент `Startup.cs`, содержащий ссылки на все сущности.

**MainCamera** - Основная камера.

**Light** - Основной источник света.

**MainCanvas** - Объект, содержащий `Canvas`, в котором находится весь UI.

**EventSystem** - Служебный объект Unity. Содержит платформо-зависимые настройки системы ввода.



## Архитектура

**Startup.cs** - Точка входа.

`_eventBus` (инжектится как EventBus) - Шина событий. Принимает события через метод `RaiseEvent`. События наследуются от класса `IEvent` и располагаются в /Scripts/Events/.

`_injectionContainer` - Реализация Dependency Injection. Внедряет экземпляры классов в поля других классов.

Регистрация зависимости осуществляется методом `RegisterInjection`.

`_automator` - Автоматизирует регистрацию подписчиков в `_injectionContainer` и `_eventBus`.

[!] Автоматизированные классы-системы должны располагаться только в /Scripts/Systems/ для удобства их поиска и отладки.

Пример класса-системы с автоматическим внедрением зависимостей и шиной событий:

```
using CodePatterns.Automatization;
using CodePatterns.Events;
using CodePatterns.InjectionContainer;
using Scripts.Events;

namespace Scripts.Systems {
    [Auto(ПОРЯДОК_ВЫЗОВА)] [Preserve]
    public class ИМЯ_КЛАССА_СИСТЕМЫ : IInjectionTarget, IEventConsumer, IEventConsumer<КЛАСС_СОБЫТИЯ> {
        public EventBus EventBus;

        public void CatchEvent(КЛАСС_СОБЫТИЯ e) {
            ...
        }
    }
}
```
